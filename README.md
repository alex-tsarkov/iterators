# alex-tsarkov/iterators

A collection of iterators and functions to deal with iterable objects.

## Installation

PHP 7.4 or greater is required. Use `composer` to include this package in your project:
```
$ composer require alex-tsarkov/iterators
```

## License

This software is released under the [MIT License](LICENSE).
