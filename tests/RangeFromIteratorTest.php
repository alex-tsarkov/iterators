<?php

namespace AlexTsarkov\Iterators;

use PHPUnit\Framework\TestCase;

/**
 * @internal
 * @covers \AlexTsarkov\Iterators\RangeFromIterator
 */
final class RangeFromIteratorTest extends TestCase
{
    /**
     * @small
     * @testWith [1, []]
     *           [-4, [-4]]
     *           [7, [7, 8, 9]]
     *           [-1, [-1, 0, 1, 2, 3]]
     *
     * @param int[] $expected
     */
    public function testIterator(int $start, array $expected): void
    {
        $iter = (new RangeFromIterator($start))->getIterator();
        $iter->rewind();

        foreach ($expected as $value) {
            $this->assertTrue($iter->valid());
            $this->assertSame($value, $iter->current());
            $iter->next();
        }
        $this->assertTrue($iter->valid());
    }
}
