<?php

namespace AlexTsarkov\Iterators;

use PHPUnit\Framework\TestCase;

/**
 * @internal
 * @covers \AlexTsarkov\Iterators\ChainIterator
 */
final class ChainIteratorTest extends TestCase
{
    /**
     * @dataProvider provideData
     *
     * @template TValue
     *
     * @param iterable<TValue>[] $data
     * @param TValue[]           $expected
     */
    public function testIterator(array $data, array $expected): void
    {
        $iter = (new ChainIterator(...$data))->getIterator();
        $iter->rewind();

        foreach ($expected as $value) {
            $this->assertTrue($iter->valid());
            $this->assertSame($value, $iter->current());
            $iter->next();
        }
        $this->assertFalse($iter->valid());
    }

    /**
     * @return iterable<array{array<iterable>, array}>
     */
    public function provideData(): iterable
    {
        $empty = static fn () => new EmptyIterator();
        $chain = static fn ($a, $b) => new ChainIterator($a, $b);

        yield [[$empty(), []], []];
        yield [[$chain([], []), []], []];
        yield [[[], [1, 2, 3]], [1, 2, 3]];
        yield [[[1, 2], [3, 4], [5]], [1, 2, 3, 4, 5]];
        yield [[[], [1, 2, 3], [], [4, 5], []], [1, 2, 3, 4, 5]];
        yield [[$chain($chain([6, 5], []), [2, 3]), $chain([1], [2])], [6, 5, 2, 3, 1, 2]];
    }
}
