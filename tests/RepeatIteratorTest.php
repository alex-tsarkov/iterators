<?php

namespace AlexTsarkov\Iterators;

use AlexTsarkov\Iterators\Stub\CloneableObject;
use PHPUnit\Framework\TestCase;

/**
 * @internal
 * @covers \AlexTsarkov\Iterators\RepeatIterator
 */
final class RepeatIteratorTest extends TestCase
{
    /**
     * @dataProvider provideData
     * @small
     *
     * @template TValue
     *
     * @param TValue           $data
     * @param iterable<TValue> $expected
     */
    public function testIterator($data, iterable $expected): void
    {
        $iter = (new RepeatIterator($data))->getIterator();
        $iter->rewind();

        foreach ($expected as $value) {
            $this->assertTrue($iter->valid());
            $this->assertSame($value, $iter->current());
            $iter->next();
        }
        $this->assertTrue($iter->valid());
    }

    /**
     * @return iterable<array{mixed, iterable}>
     */
    public function provideData(): iterable
    {
        yield [1, []];
        yield [1, [1]];
        yield [1, [1, 1]];
        yield [1, [1, 1, 1]];
        yield [2, [2, 2, 2, 2]];

        $obj = new CloneableObject();
        yield [$obj, [$obj, $obj, $obj]];
    }
}
