<?php

namespace AlexTsarkov\Iterators;

use PHPUnit\Framework\TestCase;

/**
 * @internal
 * @covers \AlexTsarkov\Iterators\BytesIterator
 */
final class BytesIteratorTest extends TestCase
{
    /**
     * @testWith ["", 0]
     *           ["English", 7]
     *           ["русский", 14]
     *           ["日本語", 9]
     */
    public function testCount(string $str, int $expected): void
    {
        $this->assertCount($expected, new BytesIterator($str));
    }

    /**
     * @testWith ["", []]
     *           ["\uFEFF", [239, 187, 191]]
     *           ["Hello", [72, 101, 108, 108, 111]]
     *           ["Привет", [208, 159, 209, 128, 208, 184, 208, 178, 208, 181, 209, 130]]
     *
     * @param int[] $expected
     */
    public function testIterator(string $str, array $expected): void
    {
        $iter = (new BytesIterator($str))->getIterator();
        $iter->rewind();

        foreach ($expected as $value) {
            $this->assertTrue($iter->valid());
            $this->assertSame($value, $iter->current());
            $iter->next();
        }
        $this->assertFalse($iter->valid());
    }
}
