<?php

namespace AlexTsarkov\Iterators;

use PHPUnit\Framework\TestCase;

/**
 * @internal
 * @covers \AlexTsarkov\Iterators\SuccessorsIterator
 */
final class SuccessorsIteratorTest extends TestCase
{
    /**
     * @dataProvider provideData
     * @small
     *
     * @template TValue
     *
     * @param ?TValue                   $data
     * @param callable(TValue): ?TValue $fn
     * @param TValue[]                  $expected
     */
    public function testIterator($data, callable $fn, array $expected): void
    {
        $iter = (new SuccessorsIterator($data, $fn))->getIterator();
        $iter->rewind();

        foreach ($expected as $value) {
            $this->assertTrue($iter->valid());
            $this->assertSame($value, $iter->current());
            $iter->next();
        }
        $this->assertFalse($iter->valid());
    }

    /**
     * @return iterable<array{?mixed, callable(mixed): ?mixed, array}>
     */
    public function provideData(): iterable
    {
        yield [null, static fn () => 1, []];
        yield [1, static fn () => null, [1]];
        yield [1, static fn ($v) => $v < 5 ? $v + 1 : null, [1, 2, 3, 4, 5]];
        yield [1, static fn ($v) => $v < 10 ? $v * 3 : null, [1, 3, 9, 27]];
    }
}
