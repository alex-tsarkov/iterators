<?php

namespace AlexTsarkov\Iterators;

use PHPUnit\Framework\TestCase;

/**
 * @internal
 * @covers \AlexTsarkov\Iterators\StepByIterator
 */
final class StepByIteratorTest extends TestCase
{
    /**
     * @dataProvider provideData
     *
     * @template TValue
     *
     * @param iterable<TValue> $data
     * @param TValue[]         $expected
     */
    public function testIterator(iterable $data, int $step, array $expected): void
    {
        $iter = (new StepByIterator($data, $step))->getIterator();
        $iter->rewind();

        foreach ($expected as $value) {
            $this->assertTrue($iter->valid());
            $this->assertSame($value, $iter->current());
            $iter->next();
        }
        $this->assertFalse($iter->valid());
    }

    /**
     * @return iterable<array{iterable, int, array}>
     */
    public function provideData(): iterable
    {
        yield [[], -1, []];
        yield [[], 0, []];
        yield [[], 1, []];
        yield [[], 2, []];
        yield [[1, 2, 3, 4, 5], -1, [1]];
        yield [[1, 2, 3, 4, 5], 0, [1]];
        yield [[1, 2, 3, 4, 5], 1, [1, 2, 3, 4, 5]];
        yield [[1, 2, 3, 4, 5], 2, [1, 3, 5]];
        yield [[1, 2, 3, 4, 5], 3, [1, 4]];
    }
}
