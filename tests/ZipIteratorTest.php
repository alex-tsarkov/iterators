<?php

namespace AlexTsarkov\Iterators;

use PHPUnit\Framework\TestCase;

/**
 * @internal
 * @covers \AlexTsarkov\Iterators\ZipIterator
 */
final class ZipIteratorTest extends TestCase
{
    /**
     * @dataProvider provideData
     *
     * @template TValue
     *
     * @param iterable<TValue>[] $data
     * @param TValue[][]         $expected
     */
    public function testIterator(array $data, array $expected): void
    {
        $iter = (new ZipIterator(...$data))->getIterator();
        $iter->rewind();

        foreach ($expected as $value) {
            $this->assertTrue($iter->valid());
            $this->assertSame($value, $iter->current());
            $iter->next();
        }
        $this->assertFalse($iter->valid());
    }

    /**
     * @return iterable<array{array<iterable>, array<array>}>
     */
    public function provideData(): iterable
    {
        $empty = new EmptyIterator();
        $fuse = static fn ($iter) => new FuseIterator($iter);

        yield [[$empty, []], []];
        yield [[[], [1, 2, 3]], []];
        yield [[[1, 2, 3], []], []];
        yield [[[1, 2, 3], $fuse([4, 5])], [[1, 4], [2, 5]]];
        yield [[[1, 2, 3], [-1, -2, -3]], [[1, -1], [2, -2], [3, -3]]];
    }
}
