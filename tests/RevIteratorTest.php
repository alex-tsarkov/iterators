<?php

namespace AlexTsarkov\Iterators;

use PHPUnit\Framework\TestCase;

/**
 * @internal
 * @covers \AlexTsarkov\Iterators\RevIterator
 */
final class RevIteratorTest extends TestCase
{
    /**
     * @dataProvider provideData
     *
     * @template TValue
     *
     * @param iterable<TValue> $data
     * @param TValue[]         $expected
     */
    public function testIterator(iterable $data, array $expected): void
    {
        $iter = (new RevIterator($data))->getIterator();
        $iter->rewind();

        foreach ($expected as $value) {
            $this->assertTrue($iter->valid());
            $this->assertSame($value, $iter->current());
            $iter->next();
        }
        $this->assertFalse($iter->valid());
    }

    /**
     * @return iterable<array{iterable, array}>
     */
    public function provideData(): iterable
    {
        $fuse = static fn ($iter) => new FuseIterator($iter);
        $rev = static fn ($iter) => new RevIterator($iter);

        yield [[], []];
        yield [[1], [1]];
        yield [[1, 2], [2, 1]];
        yield [$fuse([1, 2, 3]), [3, 2, 1]];
        yield [$rev([4, 6, 5]), [4, 6, 5]];
    }
}
