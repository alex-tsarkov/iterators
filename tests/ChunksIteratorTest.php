<?php

namespace AlexTsarkov\Iterators;

use PHPUnit\Framework\TestCase;

/**
 * @internal
 * @covers \AlexTsarkov\Iterators\ChunksIterator
 */
final class ChunksIteratorTest extends TestCase
{
    /**
     * @dataProvider provideData
     *
     * @template TValue
     *
     * @param iterable<TValue> $data
     * @param TValue[][]       $expected
     */
    public function testIterator(iterable $data, int $size, array $expected): void
    {
        $iter = (new ChunksIterator($data, $size))->getIterator();
        $iter->rewind();

        foreach ($expected as $value) {
            $this->assertTrue($iter->valid());
            $this->assertSame($value, $iter->current());
            $iter->next();
        }
        $this->assertFalse($iter->valid());
    }

    /**
     * @return iterable<array{iterable, int, array<array>}>
     */
    public function provideData(): iterable
    {
        yield [[], 1, []];
        yield [[1, 2, 3], 1, [[1], [2], [3]]];
        yield [[1, 2, 3], 2, [[1, 2], [3]]];
    }
}
