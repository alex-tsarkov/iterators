<?php

namespace AlexTsarkov\Iterators;

use AlexTsarkov\Iterators\Stub\CloneableObject;
use PHPUnit\Framework\TestCase;

/**
 * @internal
 * @covers \AlexTsarkov\Iterators\OnceWithIterator
 */
final class OnceWithIteratorTest extends TestCase
{
    /**
     * @dataProvider provideData
     *
     * @template TValue
     *
     * @param callable(): TValue $fn
     */
    public function testCount(callable $fn): void
    {
        $this->assertCount(1, new OnceWithIterator($fn));
    }

    /**
     * @dataProvider provideData
     *
     * @template TValue
     *
     * @param callable(): TValue $fn
     * @param TValue             $expected
     */
    public function testIterator(callable $fn, $expected): void
    {
        $iter = (new OnceWithIterator($fn))->getIterator();
        $iter->rewind();
        $this->assertTrue($iter->valid());
        $this->assertSame($expected, $iter->current());
        $iter->next();
        $this->assertFalse($iter->valid());
    }

    /**
     * @return iterable<array{callable(): mixed, mixed}>
     */
    public function provideData(): iterable
    {
        yield [static fn () => 1, 1];
        yield [static fn () => 2, 2];
        yield [static fn () => 3, 3];

        $obj = new CloneableObject();
        yield [static fn () => $obj, $obj];
    }
}
