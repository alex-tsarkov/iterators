<?php

namespace AlexTsarkov\Iterators;

use AlexTsarkov\Iterators\Stub\Callback;
use PHPUnit\Framework\TestCase;

/**
 * @internal
 * @covers \AlexTsarkov\Iterators\InspectIterator
 */
final class InspectIteratorTest extends TestCase
{
    /**
     * @dataProvider provideData
     *
     * @template TValue
     *
     * @param iterable<TValue> $data
     * @param TValue[]         $expected
     */
    public function testIterator(iterable $data, array $expected): void
    {
        /**
         * @var (callable(TValue): void)&\PHPUnit\Framework\MockObject\MockObject
         */
        $fn = $this->createStub(Callback::class);
        $fn->expects($this->exactly(\count($expected)))
            ->method('__invoke')
        ;

        $iter = (new InspectIterator($data, $fn))->getIterator();
        $iter->rewind();

        foreach ($expected as $value) {
            $this->assertTrue($iter->valid());
            $this->assertSame($value, $iter->current());
            $iter->next();
        }
        $this->assertFalse($iter->valid());
    }

    /**
     * @return iterable<array{iterable, array}>
     */
    public function provideData(): iterable
    {
        yield [[], []];
        yield [[1], [1]];
        yield [[1, 2], [1, 2]];
        yield [[1, 2, 3], [1, 2, 3]];
    }
}
