<?php

namespace AlexTsarkov\Iterators;

use PHPUnit\Framework\TestCase;

/**
 * @internal
 * @covers \AlexTsarkov\Iterators\KeysIterator
 */
final class KeysIteratorTest extends TestCase
{
    /**
     * @dataProvider provideData
     *
     * @template TKey
     * @template TValue
     *
     * @param iterable<TKey, TValue> $data
     * @param TKey[]                 $expected
     */
    public function testIterator(iterable $data, array $expected): void
    {
        $iter = (new KeysIterator($data))->getIterator();
        $iter->rewind();

        foreach ($expected as $value) {
            $this->assertTrue($iter->valid());
            $this->assertSame($value, $iter->current());
            $iter->next();
        }
        $this->assertFalse($iter->valid());
    }

    /**
     * @return iterable<array{iterable, array}>
     */
    public function provideData(): iterable
    {
        $keys = static fn ($iter) => new KeysIterator($iter);

        yield [[], []];
        yield [[1, 2], [0, 1]];
        yield [['a' => 1, 'b' => 2], ['a', 'b']];
        yield [$keys(['a' => 1, 'b' => 2]), [0, 1]];
    }
}
