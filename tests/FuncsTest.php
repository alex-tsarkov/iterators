<?php

namespace AlexTsarkov\Iterators;

use AlexTsarkov\Iterators\Stub\Callback;
use PHPUnit\Framework\TestCase;

/**
 * @internal
 * @coversNothing
 */
final class FuncsTest extends TestCase
{
    /**
     * @covers \AlexTsarkov\Iterators\to_iterator()
     * @testWith [[1, 2]]
     *
     * @template TValue
     *
     * @param array<TValue> $data
     */
    public function testToIterator(array $data): void
    {
        $this->assertInstanceOf(\Iterator::class, to_iterator($data));
        $this->assertInstanceOf(\Iterator::class, to_iterator(new \ArrayIterator($data)));
        $this->assertInstanceOf(\Iterator::class, to_iterator(new \ArrayObject($data)));
        $this->assertInstanceOf(\Iterator::class, to_iterator((static function () use ($data): \Generator {
            yield from $data;
        })()));
    }

    /**
     * @covers \AlexTsarkov\Iterators\collect()
     * @testWith [[], false, []]
     *           [[], true, []]
     *           [[4, 17, -3], false, [4, 17, -3]]
     *           [[4, 17, -3], true, [4, 17, -3]]
     *           [{"f": 12, "n": 5, "Z": 33}, false, [12, 5, 33]]
     *           [{"f": 12, "n": 5, "Z": 33}, true, {"f": 12, "n": 5, "Z": 33}]
     *
     * @template TValue
     *
     * @param iterable<TValue> $iter
     * @param TValue[]         $expected
     */
    public function testCollect(iterable $iter, bool $use_keys, array $expected): void
    {
        $this->assertSame($expected, collect($iter, $use_keys));
    }

    /**
     * @covers \AlexTsarkov\Iterators\count()
     * @testWith [[], 0]
     *           [[0], 1]
     *           [[-2, 13], 2]
     *           [[1, -1, 13], 3]
     *           [[-44, 33, -22, 11], 4]
     *
     * @template TValue
     *
     * @param iterable<TValue> $iter
     */
    public function testCount(iterable $iter, int $expected): void
    {
        $this->assertSame($expected, 0 + count($iter));
    }

    /**
     * @covers \AlexTsarkov\Iterators\partition()
     * @testWith [[], [[], []]]
     *           [[19, 11], [[19, 11], []]]
     *           [[8, 2, 38], [[], [8, 2, 38]]]
     *           [[13, 21, 0, 9, 6], [[13, 21, 9], [0, 6]]]
     *
     * @template TValue
     *
     * @param iterable<TValue>          $iter
     * @param array{TValue[], TValue[]} $expected
     */
    public function testPartition(iterable $iter, array $expected): void
    {
        $predicate = static fn (int $n): bool => 0 !== $n % 2;
        $pair = partition($iter, $predicate);
        $this->assertSame($expected[0], $pair[0]);
        $this->assertSame($expected[1], $pair[1]);
    }

    /**
     * @covers \AlexTsarkov\Iterators\is_partitioned()
     * @testWith [[], true]
     *           [[7, 43, 1], true]
     *           [[16, 20], true]
     *           [[6, 5, 18, 7, 13], false]
     *           [[15, 1, 4, 9, 25], false]
     *           [[8, 0, 34, 17, 19], true]
     *
     * @template TValue
     *
     * @param iterable<TValue> $iter
     */
    public function testIsPartitioned(iterable $iter, bool $expected): void
    {
        $predicate = static fn (int $n): bool => 0 === $n % 2;
        $this->assertSame($expected, is_partitioned($iter, $predicate));
    }

    /**
     * @covers \AlexTsarkov\Iterators\nth()
     * @testWith [[], 0, null]
     *           [[], -1, null]
     *           [[], 8, null]
     *           [[35, -12, 8], 0, 35]
     *           [[35, -12, 8], 1, -12]
     *           [[35, -12, 8], 2, 8]
     *           [[35, -12, 8], 3, null]
     *           [[35, -12, 8], -2, null]
     *
     * @template TValue
     *
     * @param iterable<TValue> $iter
     * @param ?TValue          $expected
     */
    public function testNth(iterable $iter, int $n, $expected): void
    {
        $this->assertSame($expected, nth($iter, $n));
    }

    /**
     * @covers \AlexTsarkov\Iterators\last()
     * @testWith [[], null]
     *           [[12], 12]
     *           [[-6, 23], 23]
     *           [[1, -1, -47], -47]
     *           [[-23, 52, 17, -9], -9]
     *
     * @template TValue
     *
     * @param iterable<TValue> $iter
     * @param ?TValue          $expected
     */
    public function testLast(iterable $iter, $expected): void
    {
        $this->assertSame($expected, last($iter));
    }

    /**
     * @covers \AlexTsarkov\Iterators\for_each()
     * @testWith [[], 0]
     *           [[2], 1]
     *           [[1, -3], 2]
     *           [[7, 4, 1], 3]
     *           [[1, 0, 1, 0], 4]
     *
     * @template TValue
     *
     * @param iterable<TValue> $iter
     */
    public function testForEach(iterable $iter, int $expected): void
    {
        /**
         * @var (callable(TValue): void)&\PHPUnit\Framework\MockObject\MockObject
         */
        $fn = $this->createStub(Callback::class);
        $fn->expects($this->exactly($expected))
            ->method('__invoke')
        ;

        for_each($iter, $fn);
    }

    /**
     * @covers \AlexTsarkov\Iterators\fold()
     * @testWith [[], -2, -2]
     *           [[16], 3, 19]
     *           [[-1, 9], 0, 8]
     *           [[1, -5, 0, 14, 2, -3], 10, 19]
     *
     * @template TValue
     * @template TAcc
     *
     * @param iterable<TValue> $iter
     * @param TAcc             $init
     * @param TAcc             $expected
     */
    public function testFold(iterable $iter, $init, $expected): void
    {
        $fn = static fn (int $acc, int $v): int => $acc + $v;
        $this->assertSame($expected, fold($iter, $init, $fn));
    }

    /**
     * @covers \AlexTsarkov\Iterators\fold_first()
     * @testWith [[], null]
     *           [[3], 3]
     *           [[8, 0], 8]
     *           [[2, 5, -3, 11, 5, -7], 13]
     *
     * @template TValue
     *
     * @param iterable<TValue> $iter
     * @param ?TValue          $expected
     */
    public function testFoldFirst(iterable $iter, $expected): void
    {
        $fn = static fn (int $acc, int $v): int => $acc + $v;
        $this->assertSame($expected, fold_first($iter, $fn));
    }

    /**
     * @covers \AlexTsarkov\Iterators\all()
     * @testWith [[], true]
     *           [[false], false]
     *           [[true], true]
     *           [[false, true], false]
     *           [[true, false], false]
     *
     * @param iterable<bool> $iter
     */
    public function testAll(iterable $iter, bool $expected): void
    {
        $predicate = static fn (bool $v): bool => $v;
        $this->assertSame($expected, all($iter, $predicate));
    }

    /**
     * @covers \AlexTsarkov\Iterators\any()
     * @testWith [[], false]
     *           [[false], false]
     *           [[true], true]
     *           [[false, true], true]
     *           [[true, false], true]
     *
     * @param iterable<bool> $iter
     */
    public function testAny(iterable $iter, bool $expected): void
    {
        $predicate = static fn (bool $v): bool => $v;
        $this->assertSame($expected, any($iter, $predicate));
    }

    /**
     * @covers \AlexTsarkov\Iterators\find()
     * @testWith [[], null]
     *           [[41, 23, 9], null]
     *           [[4, 28, 30], 4]
     *           [[31, 6, 18], 6]
     *
     * @template TValue
     *
     * @param iterable<TValue> $iter
     * @param ?TValue          $expected
     */
    public function testFind(iterable $iter, $expected): void
    {
        $predicate = static fn (int $n): bool => 0 === $n % 2;
        $this->assertSame($expected, find($iter, $predicate));
    }

    /**
     * @covers \AlexTsarkov\Iterators\find_map()
     * @testWith [[], null]
     *           [[11, 3, 13], null]
     *           [[22, 12, 14], 11]
     *           [[9, 5, 8], 4]
     *
     * @template TValue1
     * @template TValue2
     *
     * @param iterable<TValue1> $iter
     * @param ?TValue2          $expected
     */
    public function testFindMap(iterable $iter, $expected): void
    {
        $fn = static fn (int $v): ?int => 0 === $v % 2 ? $v >> 1 : null;
        $this->assertSame($expected, find_map($iter, $fn));
    }

    /**
     * @covers \AlexTsarkov\Iterators\position()
     * @testWith [[], -1]
     *           [[19, 21, 1], -1]
     *           [[14, 2, 8], 0]
     *           [[3, 12, 12], 1]
     *
     * @template TValue
     *
     * @param iterable<TValue> $iter
     */
    public function testPosition(iterable $iter, int $expected): void
    {
        $predicate = static fn (int $n): bool => 0 === $n % 2;
        $this->assertSame($expected, position($iter, $predicate));
    }

    /**
     * @covers \AlexTsarkov\Iterators\rposition()
     * @testWith [[], -1]
     *           [[24, 40, 2], -1]
     *           [[5, 15, 7], 2]
     *           [[11, 4, 11], 2]
     *
     * @template TValue
     *
     * @param iterable<TValue> $iter
     */
    public function testRposition(iterable $iter, int $expected): void
    {
        $predicate = static fn (int $n): bool => 0 !== $n % 2;
        $this->assertSame($expected, rposition($iter, $predicate));
    }

    /**
     * @covers \AlexTsarkov\Iterators\max()
     * @testWith [[], null]
     *           [[2], 2]
     *           [[14, -3, 11], 14]
     *           [[5, 7, -2, 7], 7]
     *
     * @template TValue
     *
     * @param iterable<TValue> $iter
     * @param ?TValue          $expected
     */
    public function testMax(iterable $iter, $expected): void
    {
        $this->assertSame($expected, max($iter));
    }

    /**
     * @covers \AlexTsarkov\Iterators\min()
     * @testWith [[], null]
     *           [[9], 9]
     *           [[-1, 13, -7], -7]
     *           [[1, 4, 1, 5], 1]
     *
     * @template TValue
     *
     * @param iterable<TValue> $iter
     * @param ?TValue          $expected
     */
    public function testMin(iterable $iter, $expected): void
    {
        $this->assertSame($expected, min($iter));
    }

    /**
     * @covers \AlexTsarkov\Iterators\max_by()
     * @testWith [[], null]
     *           [[2], 2]
     *           [[-9, 4, 8], -9]
     *           [[5, 7, -2, 7], 7]
     *
     * @template TValue
     *
     * @param iterable<TValue> $iter
     * @param ?TValue          $expected
     */
    public function testMaxBy(iterable $iter, $expected): void
    {
        $compare = static fn ($a, $b): int => $a * $a <=> $b * $b;
        $this->assertSame($expected, max_by($iter, $compare));
    }

    /**
     * @covers \AlexTsarkov\Iterators\min_by()
     * @testWith [[], null]
     *           [[7], 7]
     *           [[5, -8, -9], 5]
     *           [[5, 2, -9, 2], 2]
     *
     * @template TValue
     *
     * @param iterable<TValue> $iter
     * @param ?TValue          $expected
     */
    public function testMinBy(iterable $iter, $expected): void
    {
        $compare = static fn ($a, $b): int => $a * $a <=> $b * $b;
        $this->assertSame($expected, min_by($iter, $compare));
    }

    /**
     * @covers \AlexTsarkov\Iterators\max_by_key()
     * @testWith [[], null]
     *           [[3], 3]
     *           [[6, -1, 8, -7], -1]
     *
     * @template TValue
     *
     * @param iterable<TValue> $iter
     * @param ?TValue          $expected
     */
    public function testMaxByKey(iterable $iter, $expected): void
    {
        $fn = static fn ($a) => -$a * $a;
        $this->assertSame($expected, max_by_key($iter, $fn));
    }

    /**
     * @covers \AlexTsarkov\Iterators\min_by_key()
     * @testWith [[], null]
     *           [[-2], -2]
     *           [[4, 0, 9, -4], 9]
     *
     * @template TValue
     *
     * @param iterable<TValue> $iter
     * @param ?TValue          $expected
     */
    public function testMinByKey(iterable $iter, $expected): void
    {
        $fn = static fn ($a) => -$a * $a;
        $this->assertSame($expected, min_by_key($iter, $fn));
    }

    /**
     * @covers \AlexTsarkov\Iterators\sum()
     * @testWith [[], 0]
     *           [[0], 0]
     *           [[4], 4]
     *           [[0, 27], 27]
     *           [[72, -72.0], 0]
     *           [[9.2, -5, -5.5], -1.3]
     *
     * @param iterable<float|int> $iter
     * @param float|int           $expected
     */
    public function testSum(iterable $iter, $expected): void
    {
        $this->assertEquals($expected, sum($iter));
    }

    /**
     * @covers \AlexTsarkov\Iterators\product()
     * @testWith [[], 1]
     *           [[0], 0]
     *           [[0, 58], 0]
     *           [[25, 1], 25]
     *           [[1.8, 4], 7.2]
     *           [[2, 0.5], 1]
     *           [[8, -6, 0.5], -24]
     *
     * @param iterable<float|int> $iter
     * @param float|int           $expected
     */
    public function testProduct(iterable $iter, $expected): void
    {
        $this->assertEquals($expected, product($iter));
    }

    /**
     * @covers \AlexTsarkov\Iterators\unzip()
     * @testWith [[], [[], []]]
     *           [[[4, 13], [6, 9]], [[4, 6], [13, 9]]]
     *           [[["b", 1], ["k", 5], ["c", -2]], [["b", "k", "c"], [1, 5, -2]]]
     *
     * @template TValue1
     * @template TValue2
     *
     * @param iterable<array{TValue1, TValue2}> $iter
     * @param array{TValue1[], TValue2[]}       $expected
     */
    public function testUnzip(iterable $iter, array $expected): void
    {
        [$a, $b] = unzip($iter);
        $this->assertSame($expected[0], $a);
        $this->assertSame($expected[1], $b);
    }

    /**
     * @covers \AlexTsarkov\Iterators\cmp()
     * @testWith [[1], [1], 0]
     *           [[1], [1, 2], -1]
     *           [[1, 2], [1], 1]
     *
     * @template TValue
     *
     * @param iterable<TValue> $a
     * @param iterable<TValue> $b
     */
    public function testCmp(iterable $a, iterable $b, int $expected): void
    {
        $this->assertSame($expected, cmp($a, $b));
    }

    /**
     * @covers \AlexTsarkov\Iterators\cmp_by()
     * @testWith [[1, 2, 3], [1, 4, 9], 0]
     *           [[1, 2], [1, 4, 9], -1]
     *           [[1, 2, 3], [1, 4], 1]
     *
     * @template TValue
     *
     * @param iterable<TValue> $a
     * @param iterable<TValue> $b
     */
    public function testCmpBy(iterable $a, iterable $b, int $expected): void
    {
        $cmp = static fn ($x, $y): int => $x * $x <=> $y;
        $this->assertSame($expected, cmp_by($a, $b, $cmp));
    }

    /**
     * @covers \AlexTsarkov\Iterators\eq()
     * @testWith [[1], [1], true]
     *           [[1], [1, 2], false]
     *           [[1, 2], [1], false]
     *
     * @template TValue
     *
     * @param iterable<TValue> $a
     * @param iterable<TValue> $b
     */
    public function testEq(iterable $a, iterable $b, bool $expected): void
    {
        $this->assertThat(eq($a, $b), $expected ? self::isTrue() : self::isFalse());
    }

    /**
     * @covers \AlexTsarkov\Iterators\eq_by()
     * @testWith [[1, 2, 3], [1, 4, 9], true]
     *           [[1, 2], [1, 4, 9], false]
     *           [[1, 2, 3], [1, 4], false]
     *
     * @template TValue
     *
     * @param iterable<TValue> $a
     * @param iterable<TValue> $b
     */
    public function testEqBy(iterable $a, iterable $b, bool $expected): void
    {
        $eq = static fn ($x, $y): bool => $x * $x === $y;
        $this->assertThat(eq_by($a, $b, $eq), $expected ? self::isTrue() : self::isFalse());
    }

    /**
     * @covers \AlexTsarkov\Iterators\ne()
     * @testWith [[1], [1], false]
     *           [[1], [1, 2], true]
     *
     * @template TValue
     *
     * @param iterable<TValue> $a
     * @param iterable<TValue> $b
     */
    public function testNe(iterable $a, iterable $b, bool $expected): void
    {
        $this->assertThat(ne($a, $b), $expected ? self::isTrue() : self::isFalse());
    }

    /**
     * @covers \AlexTsarkov\Iterators\lt()
     * @testWith [[1], [1], false]
     *           [[1], [1, 2], true]
     *           [[1, 2], [1], false]
     *
     * @template TValue
     *
     * @param iterable<TValue> $a
     * @param iterable<TValue> $b
     */
    public function testLt(iterable $a, iterable $b, bool $expected): void
    {
        $this->assertThat(lt($a, $b), $expected ? self::isTrue() : self::isFalse());
    }

    /**
     * @covers \AlexTsarkov\Iterators\le()
     * @testWith [[1], [1], true]
     *           [[1], [1, 2], true]
     *           [[1, 2], [1], false]
     *
     * @template TValue
     *
     * @param iterable<TValue> $a
     * @param iterable<TValue> $b
     */
    public function testLe(iterable $a, iterable $b, bool $expected): void
    {
        $this->assertThat(le($a, $b), $expected ? self::isTrue() : self::isFalse());
    }

    /**
     * @covers \AlexTsarkov\Iterators\gt()
     * @testWith [[1], [1], false]
     *           [[1], [1, 2], false]
     *           [[1, 2], [1], true]
     *
     * @template TValue
     *
     * @param iterable<TValue> $a
     * @param iterable<TValue> $b
     */
    public function testGt(iterable $a, iterable $b, bool $expected): void
    {
        $this->assertThat(gt($a, $b), $expected ? self::isTrue() : self::isFalse());
    }

    /**
     * @covers \AlexTsarkov\Iterators\ge()
     * @testWith [[1], [1], true]
     *           [[1], [1, 2], false]
     *           [[1, 2], [1], true]
     *
     * @template TValue
     *
     * @param iterable<TValue> $a
     * @param iterable<TValue> $b
     */
    public function testGe(iterable $a, iterable $b, bool $expected): void
    {
        $this->assertThat(ge($a, $b), $expected ? self::isTrue() : self::isFalse());
    }

    /**
     * @covers \AlexTsarkov\Iterators\is_sorted()
     * @testWith [[1, 2, 2, 9], true]
     *           [[1, 3, 2, 4], false]
     *           [[0], true]
     *           [[], true]
     *
     * @template TValue
     *
     * @param iterable<TValue> $iter
     */
    public function testIsSorted(iterable $iter, bool $expected): void
    {
        $this->assertThat(is_sorted($iter), $expected ? self::isTrue() : self::isFalse());
    }

    /**
     * @covers \AlexTsarkov\Iterators\is_sorted_by()
     * @testWith [[1, 2, 2, 9], true]
     *           [[1, 3, 2, 4], false]
     *           [[0], true]
     *           [[], true]
     *
     * @template TValue
     *
     * @param iterable<TValue> $iter
     */
    public function testIsSortedBy(iterable $iter, bool $expected): void
    {
        $cmp = static fn ($x, $y): int => $x <=> $y;
        $this->assertThat(is_sorted_by($iter, $cmp), $expected ? self::isTrue() : self::isFalse());
    }

    /**
     * @covers \AlexTsarkov\Iterators\is_sorted_by_key()
     * @testWith [["c", "bb", "aaa"], "\\strlen", true]
     *           [[-2, -1, 0, 3], "\\abs", false]
     *
     * @template TValue
     * @template TKey
     *
     * @param iterable<TValue>       $iter
     * @param callable(TValue): TKey $fn
     */
    public function testIsSortedByKey(iterable $iter, callable $fn, bool $expected): void
    {
        $this->assertThat(is_sorted_by_key($iter, $fn), $expected ? self::isTrue() : self::isFalse());
    }
}
