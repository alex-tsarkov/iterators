<?php

namespace AlexTsarkov\Iterators;

use PHPUnit\Framework\TestCase;

/**
 * @internal
 * @covers \AlexTsarkov\Iterators\MapWhileIterator
 */
final class MapWhileIteratorTest extends TestCase
{
    /**
     * @dataProvider provideData
     *
     * @template TValue1
     * @template TValue2
     *
     * @param iterable<TValue1>           $data
     * @param callable(TValue1): ?TValue2 $predicate
     * @param TValue2[]                   $expected
     */
    public function testIterator(iterable $data, callable $predicate, array $expected): void
    {
        $iter = (new MapWhileIterator($data, $predicate))->getIterator();
        $iter->rewind();

        foreach ($expected as $value) {
            $this->assertTrue($iter->valid());
            $this->assertSame($value, $iter->current());
            $iter->next();
        }
        $this->assertFalse($iter->valid());
    }

    /**
     * @return iterable<array{iterable, callable(mixed): ?mixed, array}>
     */
    public function provideData(): iterable
    {
        $id = static fn ($v) => $v;
        $none = static fn () => null;

        yield [[], $id, []];
        yield [[1, 2, 3], $none, []];
        yield [[1, 2, 3], $id, [1, 2, 3]];
        yield [[1, 2, 3], static fn ($v) => $v + $v, [2, 4, 6]];
        yield [[1, 2, 3], static fn ($v) => $v < 3 ? $v + $v : null, [2, 4]];
    }
}
