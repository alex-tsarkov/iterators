<?php

namespace AlexTsarkov\Iterators;

use PHPUnit\Framework\TestCase;

/**
 * @internal
 * @covers \AlexTsarkov\Iterators\TakeIterator
 */
final class TakeIteratorTest extends TestCase
{
    /**
     * @dataProvider provideData
     *
     * @template TValue
     *
     * @param iterable<TValue> $data
     * @param TValue[]         $expected
     */
    public function testIterator(iterable $data, int $n, array $expected): void
    {
        $iter = (new TakeIterator($data, $n))->getIterator();
        $iter->rewind();

        foreach ($expected as $value) {
            $this->assertTrue($iter->valid());
            $this->assertSame($value, $iter->current());
            $iter->next();
        }
        $this->assertFalse($iter->valid());
    }

    /**
     * @return iterable<array{iterable, int, array}>
     */
    public function provideData(): iterable
    {
        $repeat = static fn ($value) => new RepeatIterator($value);
        $take = static fn ($iter, $n) => new TakeIterator($iter, $n);

        yield [[], 0, []];
        yield [[], 1, []];
        yield [[1, 2, 3], -1, [1, 2, 3]];
        yield [[1, 2, 3], 0, []];
        yield [[1, 2, 3], 1, [1]];
        yield [[1, 2, 3], 2, [1, 2]];
        yield [[1, 2, 3], 3, [1, 2, 3]];
        yield [[1, 2, 3], 4, [1, 2, 3]];
        yield [$take([1, 2, 3], 1), 2, [1]];
        yield [$take([1, 2, 3], 2), 1, [1]];
        yield [$repeat(0), 3, [0, 0, 0]];
    }
}
