<?php

namespace AlexTsarkov\Iterators;

use PHPUnit\Framework\TestCase;

/**
 * @internal
 * @covers \AlexTsarkov\Iterators\RepeatWithIterator
 */
final class RepeatWithIteratorTest extends TestCase
{
    /**
     * @dataProvider provideData
     * @small
     *
     * @template TValue
     *
     * @param callable(): TValue $fn
     * @param iterable<TValue>   $expected
     */
    public function testIterator(callable $fn, iterable $expected): void
    {
        $iter = (new RepeatWithIterator($fn))->getIterator();
        $iter->rewind();

        foreach ($expected as $value) {
            $this->assertTrue($iter->valid());
            $this->assertSame($value, $iter->current());
            $iter->next();
        }
        $this->assertTrue($iter->valid());
    }

    /**
     * @return iterable<array{callable(): mixed, iterable}>
     */
    public function provideData(): iterable
    {
        yield [static fn () => 1, []];
        yield [static fn () => 1, [1]];
        yield [static fn () => 1, [1, 1]];
        yield [static fn () => 1, [1, 1, 1]];
        yield [static fn () => 2, [2, 2, 2, 2]];
        yield [static function () {
            static $n = 0;

            return ++$n;
        }, [1, 2, 3, 4]];
    }
}
