<?php

namespace AlexTsarkov\Iterators;

use PHPUnit\Framework\TestCase;

/**
 * @internal
 * @covers \AlexTsarkov\Iterators\FromFnIterator
 */
final class FromFnIteratorTest extends TestCase
{
    /**
     * @dataProvider provideData
     * @small
     *
     * @template TValue
     *
     * @param callable(): ?TValue $fn
     * @param TValue[]            $expected
     */
    public function testIterator(callable $fn, array $expected): void
    {
        $iter = (new FromFnIterator($fn))->getIterator();
        $iter->rewind();

        foreach ($expected as $value) {
            $this->assertTrue($iter->valid());
            $this->assertSame($value, $iter->current());
            $iter->next();
        }
        $this->assertFalse($iter->valid());
    }

    /**
     * @return iterable<array{callable(): ?mixed, array}>
     */
    public function provideData(): iterable
    {
        $stack = [1, 2, 3, 4, 5];

        yield [static fn () => null, []];
        yield [static function () use (&$stack) {
            return \array_pop($stack);
        }, [5, 4, 3, 2, 1]];
    }
}
