<?php

namespace AlexTsarkov\Iterators;

use PHPUnit\Framework\TestCase;

/**
 * @internal
 * @covers \AlexTsarkov\Iterators\FlattenIterator
 */
final class FlattenIteratorTest extends TestCase
{
    /**
     * @dataProvider provideData
     *
     * @template TValue
     *
     * @param iterable<iterable<TValue>> $data
     * @param TValue[]                   $expected
     */
    public function testIterator(iterable $data, array $expected): void
    {
        $iter = (new FlattenIterator($data))->getIterator();
        $iter->rewind();

        foreach ($expected as $value) {
            $this->assertTrue($iter->valid());
            $this->assertSame($value, $iter->current());
            $iter->next();
        }
        $this->assertFalse($iter->valid());
    }

    /**
     * @return iterable<array{iterable<iterable|mixed>, array}>
     */
    public function provideData(): iterable
    {
        yield [[], []];
        yield [[[1, 2, 3]], [1, 2, 3]];
        yield [[[1], [2, 3]], [1, 2, 3]];
        yield [[[1, 2], [3]], [1, 2, 3]];
        yield [[[], [1], [], [2], []], [1, 2]];
    }
}
