<?php

namespace AlexTsarkov\Iterators;

use PHPUnit\Framework\TestCase;

/**
 * @internal
 * @covers \AlexTsarkov\Iterators\EnumerateIterator
 */
final class EnumerateIteratorTest extends TestCase
{
    /**
     * @dataProvider provideData
     *
     * @template TKey
     * @template TValue
     *
     * @param iterable<TKey, TValue> $data
     * @param array{TKey, TValue}[]  $expected
     */
    public function testIterator(iterable $data, array $expected): void
    {
        $iter = (new EnumerateIterator($data))->getIterator();
        $iter->rewind();

        foreach ($expected as $value) {
            $this->assertTrue($iter->valid());
            $this->assertSame($value, $iter->current());
            $iter->next();
        }
        $this->assertFalse($iter->valid());
    }

    /**
     * @return iterable<array{iterable, array<array{mixed, mixed}>}>
     */
    public function provideData(): iterable
    {
        yield [[], []];
        yield [[1, 2, 3], [[0, 1], [1, 2], [2, 3]]];
        yield [['a' => 1, 'b' => 2], [['a', 1], ['b', 2]]];
    }
}
