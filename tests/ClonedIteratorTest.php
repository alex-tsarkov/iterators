<?php

namespace AlexTsarkov\Iterators;

use AlexTsarkov\Iterators\Stub\CloneableObject;
use PHPUnit\Framework\TestCase;

/**
 * @internal
 * @covers \AlexTsarkov\Iterators\ClonedIterator
 */
final class ClonedIteratorTest extends TestCase
{
    /**
     * @dataProvider provideData
     *
     * @template TValue
     *
     * @param iterable<TValue> $data
     * @param TValue[]         $expected
     */
    public function testIterator(iterable $data, array $expected): void
    {
        $iter = (new ClonedIterator($data))->getIterator();
        $iter->rewind();

        foreach ($expected as $value) {
            $this->assertTrue($iter->valid());
            if (\is_object($value)) {
                $this->assertEquals($value, $iter->current());
                $this->assertNotSame($value, $iter->current());
            } else {
                $this->assertSame($value, $iter->current());
            }
            $iter->next();
        }
        $this->assertFalse($iter->valid());
    }

    /**
     * @return iterable<array{iterable, array}>
     */
    public function provideData(): iterable
    {
        $cloned = static fn ($iter) => new ClonedIterator($iter);

        yield [[], []];
        yield [[$a = new CloneableObject()], [clone $a]];
        yield [[1], [1]];
        yield [[$a = new CloneableObject(), 2, $b = new CloneableObject()], [$a, 2, $b]];
        yield [$cloned([16, $a = new CloneableObject()]), [16, $a]];
    }
}
