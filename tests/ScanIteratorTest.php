<?php

namespace AlexTsarkov\Iterators;

use PHPUnit\Framework\TestCase;

/**
 * @internal
 * @covers \AlexTsarkov\Iterators\ScanIterator
 */
final class ScanIteratorTest extends TestCase
{
    /**
     * @dataProvider provideData
     * @small
     *
     * @template TValue1
     * @template TValue2
     * @template TState
     *
     * @param iterable<TValue1>                    $iter
     * @param TState                               $init
     * @param callable(TSatate, TValue1): ?TValue2 $fn
     * @param TValue2[]                            $expected
     */
    public function testIterator(iterable $iter, $init, callable $fn, array $expected): void
    {
        $iter = (new ScanIterator($iter, $init, $fn))->getIterator();
        $iter->rewind();

        foreach ($expected as $value) {
            $this->assertTrue($iter->valid());
            $this->assertSame($value, $iter->current());
            $iter->next();
        }
        $this->assertFalse($iter->valid());
    }

    /**
     * @return iterable<array{iterable, mixed, callable(mixed, mixed): ?mixed, array}>
     */
    public function provideData(): iterable
    {
        yield [[], null, static fn (&$state, $v) => $v, []];
        yield [[1, 2, 3, 4], null, static fn (&$state, $v) => $v + $v, [2, 4, 6, 8]];
        yield [[1, 2, 3, 4], null, static fn () => null, []];
        yield [[1, 2, 3, 4], 2, static fn (&$state, $v) => $v > $state ? null : $state, [2, 2]];
        yield [[1, 2, 3, 4], 0, static fn (&$state, $v) => (++$state < 4) ? $v - 1 : null, [0, 1, 2]];
    }
}
