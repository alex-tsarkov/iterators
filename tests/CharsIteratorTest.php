<?php

namespace AlexTsarkov\Iterators;

use PHPUnit\Framework\TestCase;

/**
 * @internal
 * @covers \AlexTsarkov\Iterators\CharsIterator
 */
final class CharsIteratorTest extends TestCase
{
    /**
     * @dataProvider provideValidUtf8Strings
     * @dataProvider provideInvalidUtf8Strings
     *
     * @param int[] $expected
     */
    public function testIterator(string $str, array $expected): void
    {
        $iter = (new CharsIterator($str))->getIterator();
        $iter->rewind();

        foreach ($expected as $value) {
            $this->assertTrue($iter->valid());
            $this->assertSame($value, $iter->current());
            $iter->next();
        }
        $this->assertFalse($iter->valid());
    }

    /**
     * @return iterable<array{string, int[]}>
     */
    public function provideValidUtf8Strings(): iterable
    {
        yield ['', []];
        yield ['A≢Α.', [0x41, 0x2262, 0x0391, 0x2E]];
        yield ['日本語', [0x65E5, 0x672C, 0x8A9E]];
        yield ['🐘', [0x1F418]];
    }

    /**
     * @return iterable<array{string, int[]}>
     */
    public function provideInvalidUtf8Strings(): iterable
    {
        yield ["\xC0\x80", [0xFFFD, 0xFFFD]];
        yield ["/\xC0\xAE./", [0x2F, 0xFFFD, 0xFFFD, 0x2E, 0x2F]];
        yield ["\xEF", [0xFFFD]];
    }
}
