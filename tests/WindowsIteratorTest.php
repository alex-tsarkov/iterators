<?php

namespace AlexTsarkov\Iterators;

use PHPUnit\Framework\TestCase;

/**
 * @internal
 * @covers \AlexTsarkov\Iterators\WindowsIterator
 */
final class WindowsIteratorTest extends TestCase
{
    /**
     * @dataProvider provideData
     *
     * @template TValue
     *
     * @param iterable<TValue> $data
     * @param TValue[][]       $expected
     */
    public function testIterator(iterable $data, int $size, array $expected): void
    {
        $iter = (new WindowsIterator($data, $size))->getIterator();
        $iter->rewind();

        foreach ($expected as $value) {
            $this->assertTrue($iter->valid());
            $this->assertSame($value, $iter->current());
            $iter->next();
        }
        $this->assertFalse($iter->valid());
    }

    /**
     * @return iterable<array{iterable, int, array<array>}>
     */
    public function provideData(): iterable
    {
        yield [[], 1, []];
        yield [[1], 2, []];
        yield [[1, 2], 1, [[1], [2]]];
        yield [[1, 2, 3], 2, [[1, 2], [2, 3]]];
    }
}
