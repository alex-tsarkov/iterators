<?php

namespace AlexTsarkov\Iterators;

use PHPUnit\Framework\TestCase;

/**
 * @internal
 * @covers \AlexTsarkov\Iterators\CycleIterator
 */
final class CycleIteratorTest extends TestCase
{
    /**
     * @small
     */
    public function testEmptyIterator(): void
    {
        $iter = new CycleIterator(new EmptyIterator());
        $this->assertFalse($iter->getIterator()->valid());
    }

    /**
     * @dataProvider provideData
     * @small
     *
     * @template TValue
     *
     * @param iterable<TValue> $data
     * @param TValue[]         $expected
     */
    public function testIterator(iterable $data, array $expected): void
    {
        $iter = (new CycleIterator($data))->getIterator();
        $iter->rewind();

        foreach ($expected as $value) {
            $this->assertTrue($iter->valid());

            $this->assertSame($value, $iter->current());
            $iter->next();
        }
        $this->assertTrue($iter->valid());
    }

    /**
     * @return iterable<array{iterable, array}>
     */
    public function provideData(): iterable
    {
        $cycle = static fn ($iter) => new CycleIterator($iter);

        yield [[1], []];
        yield [[1], [1, 1, 1]];
        yield [[1, 2], [1, 2, 1, 2, 1]];
        yield [$cycle([1, 2]), [1, 2, 1, 2]];
    }
}
