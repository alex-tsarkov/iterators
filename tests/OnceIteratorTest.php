<?php

namespace AlexTsarkov\Iterators;

use AlexTsarkov\Iterators\Stub\CloneableObject;
use PHPUnit\Framework\TestCase;

/**
 * @internal
 * @covers \AlexTsarkov\Iterators\OnceIterator
 */
final class OnceIteratorTest extends TestCase
{
    /**
     * @dataProvider provideData
     *
     * @template TValue
     *
     * @param TValue $data
     */
    public function testCount($data): void
    {
        $this->assertCount(1, new OnceIterator($data));
    }

    /**
     * @dataProvider provideData
     *
     * @template TValue
     *
     * @param TValue $data
     * @param TValue $expected
     */
    public function testIterator($data, $expected): void
    {
        $iter = (new OnceIterator($data))->getIterator();
        $iter->rewind();
        $this->assertTrue($iter->valid());
        $this->assertSame($expected, $iter->current());
        $iter->next();
        $this->assertFalse($iter->valid());
    }

    /**
     * @return iterable<array{mixed, mixed}>
     */
    public function provideData(): iterable
    {
        yield [1, 1];
        yield [2, 2];
        yield [3, 3];

        $obj = new CloneableObject();
        yield [$obj, $obj];
    }
}
