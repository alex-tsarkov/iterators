<?php

namespace AlexTsarkov\Iterators;

use PHPUnit\Framework\TestCase;

/**
 * @internal
 * @covers \AlexTsarkov\Iterators\MapIterator
 */
final class MapIteratorTest extends TestCase
{
    /**
     * @dataProvider provideData
     *
     * @template TValue1
     * @template TValue2
     *
     * @param iterable<TValue1>          $data
     * @param callable(TValue1): TValue2 $fn
     * @param TValue2[]                  $expected
     */
    public function testIterator(iterable $data, callable $fn, array $expected): void
    {
        $iter = (new MapIterator($data, $fn))->getIterator();
        $iter->rewind();

        foreach ($expected as $value) {
            $this->assertTrue($iter->valid());
            $this->assertSame($value, $iter->current());
            $iter->next();
        }
        $this->assertFalse($iter->valid());
    }

    /**
     * @return iterable<array{iterable, callable(mixed): mixed, array}>
     */
    public function provideData(): iterable
    {
        $id = static fn ($v) => $v;

        yield [[], $id, []];
        yield [[1, 2, 3], $id, [1, 2, 3]];
        yield [[1, 2, 3], static fn ($v) => $v + $v, [2, 4, 6]];
    }
}
