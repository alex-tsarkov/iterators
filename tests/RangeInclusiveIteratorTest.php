<?php

namespace AlexTsarkov\Iterators;

use PHPUnit\Framework\TestCase;

/**
 * @internal
 * @covers \AlexTsarkov\Iterators\RangeInclusiveIterator
 */
final class RangeInclusiveIteratorTest extends TestCase
{
    /**
     * @testWith [3, 3, 1]
     *           [8, 1, 0]
     *           [11, 12, 2]
     *           [-3, 6, 10]
     */
    public function testCount(int $start, int $end, int $expected): void
    {
        $this->assertCount($expected, new RangeInclusiveIterator($start, $end));
    }

    /**
     * @testWith [-4, -4, [-4]]
     *           [13, -1, []]
     *           [6, 7, [6, 7]]
     *           [-1, 3, [-1, 0, 1, 2, 3]]
     *
     * @param int[] $expected
     */
    public function testIterator(int $start, int $end, array $expected): void
    {
        $iter = (new RangeInclusiveIterator($start, $end))->getIterator();
        $iter->rewind();

        foreach ($expected as $value) {
            $this->assertTrue($iter->valid());
            $this->assertSame($value, $iter->current());
            $iter->next();
        }
        $this->assertFalse($iter->valid());
    }
}
