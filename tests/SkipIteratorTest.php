<?php

namespace AlexTsarkov\Iterators;

use PHPUnit\Framework\TestCase;

/**
 * @internal
 * @covers \AlexTsarkov\Iterators\SkipIterator
 */
final class SkipIteratorTest extends TestCase
{
    /**
     * @dataProvider provideData
     *
     * @template TValue
     *
     * @param iterable<TValue> $data
     * @param TValue[]         $expected
     */
    public function testIterator(iterable $data, int $n, array $expected): void
    {
        $iter = (new SkipIterator($data, $n))->getIterator();
        $iter->rewind();

        foreach ($expected as $value) {
            $this->assertTrue($iter->valid());
            $this->assertSame($value, $iter->current());
            $iter->next();
        }
        $this->assertFalse($iter->valid());
    }

    /**
     * @return iterable<array{iterable, int, array}>
     */
    public function provideData(): iterable
    {
        $skip = static fn ($iter, $n) => new SkipIterator($iter, $n);

        yield [[], 0, []];
        yield [[], 1, []];
        yield [[1, 2, 3], -1, []];
        yield [[1, 2, 3], 0, [1, 2, 3]];
        yield [[1, 2, 3], 1, [2, 3]];
        yield [[1, 2, 3], 2, [3]];
        yield [[1, 2, 3], 3, []];
        yield [[1, 2, 3], 4, []];
        yield [$skip([1, 2, 3], 1), 2, [3]];
        yield [$skip([1, 2, 3], 2), 1, [3]];
    }
}
