<?php

namespace AlexTsarkov\Iterators;

use PHPUnit\Framework\TestCase;

/**
 * @internal
 * @covers \AlexTsarkov\Iterators\FilterIterator
 */
final class FilterIteratorTest extends TestCase
{
    /**
     * @dataProvider provideData
     *
     * @template TValue
     *
     * @param iterable<TValue>       $data
     * @param callable(TValue): bool $predicate
     * @param TValue[]               $expected
     */
    public function testIterator(iterable $data, callable $predicate, array $expected): void
    {
        $iter = (new FilterIterator($data, $predicate))->getIterator();
        $iter->rewind();

        foreach ($expected as $value) {
            $this->assertTrue($iter->valid());

            $this->assertSame($value, $iter->current());
            $iter->next();
        }
        $this->assertFalse($iter->valid());
    }

    /**
     * @return iterable<array{iterable, callable(mixed): bool, array}>
     */
    public function provideData(): iterable
    {
        $any = static fn (): bool => true;
        $none = static fn (): bool => false;
        $even = static fn (int $v): bool => 0 !== $v % 2;

        yield [[], $any, []];
        yield [[], $none, []];
        yield [[1, 2, 3], $any, [1, 2, 3]];
        yield [[1, 2, 3], $none, []];
        yield [[1, 2, 3, 4, 5], $even, [1, 3, 5]];
    }
}
