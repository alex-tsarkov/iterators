<?php

namespace AlexTsarkov\Iterators;

use PHPUnit\Framework\TestCase;

/**
 * @internal
 * @covers \AlexTsarkov\Iterators\ValuesIterator
 */
final class ValuesIteratorTest extends TestCase
{
    /**
     * @dataProvider provideData
     *
     * @template TValue
     *
     * @param iterable<TValue> $data
     * @param TValue[]         $expected
     */
    public function testIterator(iterable $data, array $expected): void
    {
        $iter = (new ValuesIterator($data))->getIterator();
        $iter->rewind();

        foreach ($expected as $value) {
            $this->assertTrue($iter->valid());
            $this->assertSame($value, $iter->current());
            $iter->next();
        }
        $this->assertFalse($iter->valid());
    }

    /**
     * @return iterable<array{iterable, array}>
     */
    public function provideData(): iterable
    {
        $values = static fn ($iter) => new ValuesIterator($iter);

        yield [[], []];
        yield [[1], [1]];
        yield [['a' => 1, 'b' => 2], [1, 2]];
        yield [$values([1, 2, 3]), [1, 2, 3]];
    }
}
