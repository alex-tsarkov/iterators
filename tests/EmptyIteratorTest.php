<?php

namespace AlexTsarkov\Iterators;

use PHPUnit\Framework\TestCase;

/**
 * @internal
 * @covers \AlexTsarkov\Iterators\EmptyIterator
 */
final class EmptyIteratorTest extends TestCase
{
    public function testCount(): void
    {
        $this->assertCount(0, new EmptyIterator());
    }

    public function testIterator(): void
    {
        $iter = (new EmptyIterator())->getIterator();
        $iter->rewind();
        $this->assertFalse($iter->valid());
    }
}
