<?php

namespace AlexTsarkov\Iterators;

use PHPUnit\Framework\TestCase;

/**
 * @internal
 * @covers \AlexTsarkov\Iterators\RangeIterator
 */
final class RangeIteratorTest extends TestCase
{
    /**
     * @testWith [-2, -2, 0]
     *           [9, 5, 0]
     *           [3, 4, 1]
     *           [-7, 11, 18]
     */
    public function testCount(int $start, int $end, int $expected): void
    {
        $this->assertCount($expected, new RangeIterator($start, $end));
    }

    /**
     * @testWith [6, 6, []]
     *           [10, 3, []]
     *           [4, 5, [4]]
     *           [-3, 2, [-3, -2, -1, 0, 1]]
     *
     * @param int[] $expected
     */
    public function testIterator(int $start, int $end, array $expected): void
    {
        $iter = (new RangeIterator($start, $end))->getIterator();
        $iter->rewind();

        foreach ($expected as $value) {
            $this->assertTrue($iter->valid());
            $this->assertSame($value, $iter->current());
            $iter->next();
        }
        $this->assertFalse($iter->valid());
    }
}
