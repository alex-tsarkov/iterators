<?php

namespace AlexTsarkov\Iterators\Stub;

/**
 * @internal
 */
final class CloneableObject
{
    private int $id;

    public function __construct()
    {
        $this->id = \spl_object_id($this);
    }
}
