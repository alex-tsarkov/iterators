<?php

namespace AlexTsarkov\Iterators\Stub;

/**
 * @internal
 *
 * @template TReturn
 */
abstract class Callback
{
    /**
     * @return TReturn
     */
    abstract public function __invoke();
}
