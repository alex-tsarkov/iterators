<?php

namespace AlexTsarkov\Iterators;

use PHPUnit\Framework\TestCase;

/**
 * @internal
 * @covers \AlexTsarkov\Iterators\FuseIterator
 */
final class FuseIteratorTest extends TestCase
{
    /**
     * @dataProvider provideData
     *
     * @template TValue
     *
     * @param iterable<int, ?TValue> $data
     * @param TValue[]               $expected
     */
    public function testIterator(iterable $data, array $expected): void
    {
        $iter = (new FuseIterator($data))->getIterator();
        $iter->rewind();

        foreach ($expected as $value) {
            $this->assertTrue($iter->valid());
            $this->assertSame($value, $iter->current());
            $iter->next();
        }
        $this->assertFalse($iter->valid());
    }

    /**
     * @return iterable<array{iterable, array}>
     */
    public function provideData(): iterable
    {
        $fuse = static fn ($iter) => new FuseIterator($iter);

        yield [[], []];
        yield [[null], []];
        yield [[null, 1, 2, 3], []];
        yield [[1, null, 2, 3], [1]];
        yield [[1, 2, null, 3], [1, 2]];
        yield [[1, null, 2, 3, null], [1]];
        yield [$fuse([1, 2, 3, null]), [1, 2, 3]];
        yield [$fuse([1, 2, 3]), [1, 2, 3]];
    }
}
