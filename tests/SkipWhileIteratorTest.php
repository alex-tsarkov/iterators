<?php

namespace AlexTsarkov\Iterators;

use PHPUnit\Framework\TestCase;

/**
 * @internal
 * @covers \AlexTsarkov\Iterators\SkipWhileIterator
 */
final class SkipWhileIteratorTest extends TestCase
{
    /**
     * @dataProvider provideData
     *
     * @template TValue
     *
     * @param iterable<TValue>       $data
     * @param callable(TValue): bool $predicate
     * @param TValue[]               $expected
     */
    public function testIterator(iterable $data, callable $predicate, array $expected): void
    {
        $iter = (new SkipWhileIterator($data, $predicate))->getIterator();
        $iter->rewind();

        foreach ($expected as $value) {
            $this->assertTrue($iter->valid());
            $this->assertSame($value, $iter->current());
            $iter->next();
        }
        $this->assertFalse($iter->valid());
    }

    /**
     * @return iterable<array{iterable, callable(mixed, int): bool, array}>
     */
    public function provideData(): iterable
    {
        $any = static fn (): bool => true;
        $none = static fn (): bool => false;

        yield [[], $any, []];
        yield [[], $none, []];
        yield [[0, 1, 2, 3], $any, []];
        yield [[0, 1, 2, 3], $none, [0, 1, 2, 3]];
        yield [[1, 2, 3, 4, 5], static fn (int $v) => $v < 3, [3, 4, 5]];
        yield [[1, 2, 3, 4, 5], static fn (int $v) => $v > 3, [1, 2, 3, 4, 5]];
    }
}
