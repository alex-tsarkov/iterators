<?php

namespace AlexTsarkov\Iterators;

/**
 * An iterator that iterates other iterables simultaneously.
 *
 * @template TValue1
 * @template TValue2
 * @implements \IteratorAggregate<int, array{TValue1, TValue2}>
 */
final class ZipIterator implements \IteratorAggregate
{
    /**
     * @var array{iterable<TValue1>, iterable<TValue2>}
     */
    private array $iters;

    /**
     * @param iterable<TValue1> $iter
     * @param iterable<TValue2> $other
     */
    public function __construct(iterable $iter, iterable $other)
    {
        $this->iters = [$iter, $other];
    }

    /**
     * @return \Iterator<int, array{TValue1, TValue2}>
     */
    public function getIterator(): \Iterator
    {
        $other = to_iterator($this->iters[1]);
        $other->rewind();

        foreach ($this->iters[0] as $value) {
            if (!$other->valid()) {
                break;
            }
            yield [$value, $other->current()];
            $other->next();
        }
    }
}
