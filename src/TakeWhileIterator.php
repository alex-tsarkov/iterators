<?php

namespace AlexTsarkov\Iterators;

/**
 * An iterator that only accepts values while predicate $fn returns true.
 *
 * @template TValue
 * @implements \IteratorAggregate<int, TValue>
 */
final class TakeWhileIterator implements \IteratorAggregate
{
    /**
     * @var iterable<TValue>
     */
    private iterable $iter;

    /**
     * @var callable(TValue): bool
     */
    private $fn;

    /**
     * @param iterable<TValue>       $iter
     * @param callable(TValue): bool $fn
     */
    public function __construct(iterable $iter, callable $fn)
    {
        $this->iter = $iter;
        $this->fn = $fn;
    }

    /**
     * @return \Iterator<int, TValue>
     */
    public function getIterator(): \Iterator
    {
        foreach ($this->iter as $key => $value) {
            if (!($this->fn)($_ = $value)) {
                break;
            }
            yield $key => $value;
        }
    }
}
