<?php

namespace AlexTsarkov\Iterators;

/**
 * An iterator that repeats values endlessly by applying the provided $fn.
 *
 * @template TValue
 * @implements \IteratorAggregate<int, TValue>
 */
final class RepeatWithIterator implements \IteratorAggregate
{
    /**
     * @var callable(): TValue
     */
    private $fn;

    /**
     * @param callable(): TValue $fn
     */
    public function __construct($fn)
    {
        $this->fn = $fn;
    }

    /**
     * @return \Iterator<int, TValue>
     */
    public function getIterator(): \Iterator
    {
        while (true) {
            yield ($this->fn)();
        }
        // @codeCoverageIgnoreStart
    }

    // @codeCoverageIgnoreEnd
}
