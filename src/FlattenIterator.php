<?php

namespace AlexTsarkov\Iterators;

/**
 * An iterator that flattens one level of nesting in $iter.
 *
 * @template TValue
 * @implements \IteratorAggregate<TValue>
 */
final class FlattenIterator implements \IteratorAggregate
{
    /**
     * @var iterable<iterable<TValue>>
     */
    private iterable $iter;

    /**
     * @param iterable<iterable<TValue>> $iter
     */
    public function __construct(iterable $iter)
    {
        $this->iter = $iter;
    }

    /**
     * @return \Iterator<TValue>
     */
    public function getIterator(): \Iterator
    {
        foreach ($this->iter as $value) {
            \assert(\is_iterable($value), '$value is iterable');
            yield from $value;
        }
    }
}
