<?php

namespace AlexTsarkov\Iterators;

/**
 * An iterator that only accepts values while predicate $fn returns true.
 *
 * @template TValue
 * @implements \IteratorAggregate<TValue>
 */
final class SkipWhileIterator implements \IteratorAggregate
{
    /**
     * @var iterable<TValue>
     */
    private iterable $iter;

    /**
     * @var callable(TValue): bool
     */
    private $fn;

    /**
     * @param iterable<TValue>       $iter
     * @param callable(TValue): bool $fn
     */
    public function __construct(iterable $iter, callable $fn)
    {
        $this->iter = $iter;
        $this->fn = $fn;
    }

    /**
     * @return \Iterator<TValue>
     */
    public function getIterator(): \Iterator
    {
        $skip = true;
        foreach ($this->iter as $key => $value) {
            $skip = $skip && ($this->fn)($_ = $value);
            if (!$skip) {
                yield $key => $value;
            }
        }
    }
}
