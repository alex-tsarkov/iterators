<?php

namespace AlexTsarkov\Iterators;

/**
 * An iterator that links $iters in a chain.
 *
 * @template TValue
 * @implements \IteratorAggregate<TValue>
 */
final class ChainIterator implements \IteratorAggregate
{
    /**
     * @var array<int, iterable<TValue>>
     */
    private array $iters = [];

    /**
     * @param iterable<TValue> $iter
     * @param iterable<TValue> $other
     * @param iterable<TValue> ...$others
     */
    public function __construct(iterable $iter, iterable $other, iterable ...$others)
    {
        $this->append($iter, $other, ...$others);
    }

    /**
     * @return \Iterator<TValue>
     */
    public function getIterator(): \Iterator
    {
        foreach ($this->iters as $iter) {
            yield from $iter;
        }
    }

    /**
     * @param iterable<TValue> ...$iters
     */
    private function append(iterable ...$iters): void
    {
        foreach ($iters as $iter) {
            if ($iter instanceof self) {
                $this->append(...$iter->iters);
            } else {
                $this->iters[] = $iter;
            }
        }
    }
}
