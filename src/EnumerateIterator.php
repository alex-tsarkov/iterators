<?php

namespace AlexTsarkov\Iterators;

/**
 * An iterator that yields the current key and the value during iteration.
 *
 * @template TKey
 * @template TValue
 * @implements \IteratorAggregate<array{TKey, TValue}>
 */
final class EnumerateIterator implements \IteratorAggregate
{
    /**
     * @var iterable<TKey, TValue>
     */
    private iterable $iter;

    /**
     * @param iterable<TKey, TValue> $iter
     */
    public function __construct(iterable $iter)
    {
        $this->iter = $iter;
    }

    /**
     * @return \Iterator<int, array{TKey, TValue}>
     */
    public function getIterator(): \Iterator
    {
        foreach ($this->iter as $key => $value) {
            yield [$key, $value];
        }
    }
}
