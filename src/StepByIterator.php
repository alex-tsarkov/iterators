<?php

namespace AlexTsarkov\Iterators;

/**
 * An iterator for stepping $iter by $step.
 *
 * @template TValue
 * @implements \IteratorAggregate<TValue>
 */
final class StepByIterator implements \IteratorAggregate
{
    /**
     * @var iterable<TValue>
     */
    private iterable $iter;

    private int $step;

    /**
     * @param iterable<TValue> $iter
     */
    public function __construct(iterable $iter, int $step)
    {
        \assert($step > 0, "step by \$step > 0 values, but {$step} given");

        $this->iter = $iter;
        $this->step = $step;
    }

    /**
     * @return \Iterator<TValue>
     */
    public function getIterator(): \Iterator
    {
        $n = 0;
        foreach ($this->iter as $key => $value) {
            if (0 === $n) {
                yield $key => $value;
                $n = $this->step;
            }
            --$n;
        }
    }
}
