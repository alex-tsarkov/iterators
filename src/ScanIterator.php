<?php

namespace AlexTsarkov\Iterators;

/**
 * An iterator to maintain $state while iterating another $iter.
 *
 * @template TValue
 * @template TValue2
 * @template TState
 * @implements \IteratorAggregate<TValue2>
 */
final class ScanIterator implements \IteratorAggregate
{
    /**
     * @var iterable<TValue>
     */
    private iterable $iter;

    /**
     * @var TState
     */
    private $init;

    /**
     * @var callable(TState, TValue): ?TValue2
     */
    private $fn;

    /**
     * @param iterable<TValue>                   $iter
     * @param TState                             $init
     * @param callable(TState, TValue): ?TValue2 $fn
     */
    public function __construct(iterable $iter, $init, callable $fn)
    {
        $this->iter = $iter;
        $this->init = $init;
        $this->fn = $fn;
    }

    /**
     * @return \Iterator<TValue2>
     */
    public function getIterator(): \Iterator
    {
        $state = $this->init;
        foreach ($this->iter as $key => $value) {
            $value = ($this->fn)($state, $value);
            if (null === $value) {
                break;
            }
            yield $key => $value;
        }
    }
}
