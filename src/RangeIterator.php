<?php

namespace AlexTsarkov\Iterators;

/**
 * A (half-open) range bounded inclusively below and exclusively above.
 *
 * @implements \IteratorAggregate<int, int>
 */
final class RangeIterator implements \Countable, \IteratorAggregate
{
    private int $start;

    private int $end;

    public function __construct(int $start, int $end)
    {
        $this->start = $start;
        $this->end = $end;
    }

    public function count(): int
    {
        return \max($this->end - $this->start, 0);
    }

    /**
     * @return \Iterator<int, int>
     */
    public function getIterator(): \Iterator
    {
        for ($n = $this->start; $n < $this->end; ++$n) {
            yield $n;
        }
    }
}
