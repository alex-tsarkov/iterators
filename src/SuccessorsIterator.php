<?php

namespace AlexTsarkov\Iterators;

/**
 * An iterator where each successive value is computed based on the preceding one.
 *
 * @template TValue
 * @implements \IteratorAggregate<int, TValue>
 */
final class SuccessorsIterator implements \IteratorAggregate
{
    /**
     * @var ?TValue
     */
    private $first;

    /**
     * @var callable(TValue): ?TValue
     */
    private $fn;

    /**
     * @param ?TValue $first
     */
    public function __construct($first, callable $fn)
    {
        $this->first = $first;
        $this->fn = $fn;
    }

    /**
     * @return \Iterator<int, TValue>
     */
    public function getIterator(): \Iterator
    {
        $value = $this->first;
        while (null !== $value) {
            yield $value;
            $value = ($this->fn)($value);
        }
    }
}
