<?php

namespace AlexTsarkov\Iterators;

/**
 * An iterator where each iteration calls the provided $fn.
 *
 * @template TValue
 * @implements \IteratorAggregate<int, TValue>
 */
final class FromFnIterator implements \IteratorAggregate
{
    /**
     * @var callable(): ?TValue
     */
    private $fn;

    /**
     * @param callable(): ?TValue $fn
     */
    public function __construct(callable $fn)
    {
        $this->fn = $fn;
    }

    /**
     * @return \Iterator<int, TValue>
     */
    public function getIterator(): \Iterator
    {
        $value = ($this->fn)();
        while (null !== $value) {
            yield $value;
            $value = ($this->fn)();
        }
    }
}
