<?php

namespace AlexTsarkov\Iterators;

/**
 * A range bounded inclusively below and above.
 *
 * @implements \IteratorAggregate<int, int>
 */
final class RangeInclusiveIterator implements \Countable, \IteratorAggregate
{
    private int $start;

    private int $end;

    public function __construct(int $start, int $end)
    {
        $this->start = $start;
        $this->end = $end;
    }

    public function count(): int
    {
        return \max($this->end - $this->start + 1, 0);
    }

    /**
     * @return \Iterator<int, int>
     */
    public function getIterator(): \Iterator
    {
        for ($n = $this->start; $n <= $this->end; ++$n) {
            yield $n;
        }
    }
}
