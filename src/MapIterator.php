<?php

namespace AlexTsarkov\Iterators;

/**
 * An iterator that maps the values of $iter with $fn.
 *
 * @template TValue1
 * @template TValue2
 * @implements \IteratorAggregate<TValue2>
 */
final class MapIterator implements \IteratorAggregate
{
    /**
     * @var iterable<TValue1>
     */
    private iterable $iter;

    /**
     * @var callable(TValue1): TValue2
     */
    private $fn;

    /**
     * @param iterable<TValue1>          $iter
     * @param callable(TValue1): TValue2 $fn
     */
    public function __construct(iterable $iter, callable $fn)
    {
        $this->iter = $iter;
        $this->fn = $fn;
    }

    /**
     * @return \Iterator<TValue2>
     */
    public function getIterator(): \Iterator
    {
        foreach ($this->iter as $key => $value) {
            yield $key => ($this->fn)($value);
        }
    }
}
