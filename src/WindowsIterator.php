<?php

namespace AlexTsarkov\Iterators;

/**
 * An iterator over overlapping windows of length $size.
 *
 * @template TValue
 * @implements \IteratorAggregate<int, array<int, TValue>>
 */
final class WindowsIterator implements \IteratorAggregate
{
    /**
     * @var iterable<TValue>
     */
    private iterable $iter;

    private int $size;

    /**
     * @param iterable<TValue> $iter
     */
    public function __construct(iterable $iter, int $size)
    {
        \assert($size > 0, "window \$size > 0, but {$size} given");

        $this->iter = $iter;
        $this->size = $size;
    }

    /**
     * @return \Iterator<int, array<int, TValue>>
     */
    public function getIterator(): \Iterator
    {
        $window = [];
        $n = 0;
        foreach ($this->iter as $value) {
            $window[] = $value;
            if ($this->size <= ++$n) {
                yield $window;
                \array_shift($window);
            }
        }
    }
}
