<?php

namespace AlexTsarkov\Iterators;

/**
 * An iterator over the keys of an $iter.
 *
 * @template TKey
 * @template TValue
 * @implements \IteratorAggregate<int, TKey>
 */
final class KeysIterator implements \IteratorAggregate
{
    /**
     * @var iterable<TKey, TValue>
     */
    private iterable $iter;

    /**
     * @param iterable<TKey, TValue> $iter
     */
    public function __construct(iterable $iter)
    {
        $this->iter = $iter;
    }

    /**
     * @return \Iterator<int, TKey>
     */
    public function getIterator(): \Iterator
    {
        foreach ($this->iter as $key => $_) {
            yield $key;
        }
    }
}
