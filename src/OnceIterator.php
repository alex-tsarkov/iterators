<?php

namespace AlexTsarkov\Iterators;

/**
 * An iterator that yields a $value exactly once.
 *
 * @template TValue
 * @implements \IteratorAggregate<int, TValue>
 */
final class OnceIterator implements \Countable, \IteratorAggregate
{
    /**
     * @var TValue
     */
    private $value;

    /**
     * @param TValue $value
     */
    public function __construct($value)
    {
        $this->value = $value;
    }

    public function count(): int
    {
        return 1;
    }

    /**
     * @return \Iterator<int, TValue>
     */
    public function getIterator(): \Iterator
    {
        yield $this->value;
    }
}
