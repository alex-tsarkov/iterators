<?php

namespace AlexTsarkov\Iterators;

/**
 * @implements \IteratorAggregate<int, int>
 */
final class BytesIterator implements \Countable, \IteratorAggregate
{
    private string $str;

    public function __construct(string $str)
    {
        $this->str = $str;
    }

    public function count(): int
    {
        return \strlen($this->str);
    }

    /**
     * @return \Iterator<int, int>
     */
    public function getIterator(): \Iterator
    {
        $bytes = \unpack('C*', $this->str);
        \assert(false !== $bytes, '$str is unpacked to bytes array');
        yield from \array_values($bytes);
    }
}
