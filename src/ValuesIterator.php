<?php

namespace AlexTsarkov\Iterators;

/**
 * An iterator over the values of an $iter.
 *
 * @template TValue
 * @implements \IteratorAggregate<int, TValue>
 */
final class ValuesIterator implements \IteratorAggregate
{
    /**
     * @var iterable<TValue>
     */
    private iterable $iter;

    /**
     * @param iterable<TValue> $iter
     */
    public function __construct(iterable $iter)
    {
        if ($iter instanceof self) {
            $iter = $iter->iter;
        }
        $this->iter = $iter;
    }

    /**
     * @return \Iterator<int, TValue>
     */
    public function getIterator(): \Iterator
    {
        foreach ($this->iter as $value) {
            yield $value;
        }
    }
}
