<?php

namespace AlexTsarkov\Iterators;

/**
 * An iterator that calls a $fn with a value before yielding it.
 *
 * @template TValue
 * @implements \IteratorAggregate<TValue>
 */
final class InspectIterator implements \IteratorAggregate
{
    /**
     * @var iterable<TValue>
     */
    private iterable $iter;

    /**
     * @var callable(TValue): void
     */
    private $fn;

    /**
     * @param iterable<TValue>       $iter
     * @param callable(TValue): void $fn
     */
    public function __construct(iterable $iter, callable $fn)
    {
        $this->iter = $iter;
        $this->fn = $fn;
    }

    /**
     * @return \Iterator<TValue>
     */
    public function getIterator(): \Iterator
    {
        foreach ($this->iter as $key => $value) {
            ($this->fn)($_ = $value);
            yield $key => $value;
        }
    }
}
