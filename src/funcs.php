<?php

namespace AlexTsarkov\Iterators;

/**
 * Transforms an $iter into an Iterator.
 *
 * @template TValue
 *
 * @param iterable<TValue> $iter
 *
 * @return \Iterator<TValue>
 */
function to_iterator(iterable $iter): \Iterator
{
    if (\is_array($iter)) {
        $iter = new \ArrayIterator($iter);
    } elseif ($iter instanceof \IteratorAggregate) {
        $iter = $iter->getIterator();
    }

    \assert($iter instanceof \Iterator, '$iter is Iterator');

    return $iter;
}

/**
 * Transforms an $iter into an array.
 *
 * @template TValue
 *
 * @param iterable<TValue> $iter
 *
 * @return array<TValue>
 */
function collect(iterable $iter, bool $use_keys = false): array
{
    $array = [];
    if ($use_keys) {
        foreach ($iter as $key => $value) {
            $array[$key] = $value;
        }
    } else {
        foreach ($iter as $value) {
            $array[] = $value;
        }
    }

    return $array;
}

/**
 * Consumes the $iter, counting the number of iterations and returning it.
 *
 * @template TValue
 *
 * @param iterable<TValue> $iter
 */
function count(iterable $iter): int
{
    $count = 0;
    foreach ($iter as $_) {
        ++$count;
    }

    return $count;
}

/**
 * Consumes the $iter, creating two collections from it.
 *
 * @template TValue
 *
 * @param iterable<TValue>       $iter
 * @param callable(TValue): bool $predicate
 *
 * @return array{array<int, TValue>, array<int, TValue>}
 */
function partition(iterable $iter, callable $predicate): array
{
    $a = $b = [];
    foreach ($iter as $value) {
        if ($predicate($_ = $value)) {
            $a[] = $value;
        } else {
            $b[] = $value;
        }
    }

    return [$a, $b];
}

/**
 * Checks if the elements of an $iter are partitioned according to the given $predicate.
 *
 * @template TValue
 *
 * @param iterable<TValue>       $iter
 * @param callable(TValue): bool $predicate
 */
function is_partitioned(iterable $iter, callable $predicate): bool
{
    $parted = true;
    foreach ($iter as $value) {
        if ($predicate($value)) {
            if (!$parted) {
                return false;
            }
        } else {
            $parted = false;
        }
    }

    return true;
}

/**
 * Returns the $n​th element of the $iter.
 *
 * @template TValue
 *
 * @param iterable<TValue> $iter
 *
 * @return ?TValue
 */
function nth(iterable $iter, int $n)
{
    \assert($n >= 0, "get value at \$n >= 0, but {$n} given");

    foreach ($iter as $value) {
        \assert(null !== $value, '$value is not null');
        if (0 === $n) {
            return $value;
        }
        --$n;
    }

    return null;
}

/**
 * Consumes the $iter, returning the last element.
 *
 * @template TValue
 *
 * @param iterable<TValue> $iter
 *
 * @return ?TValue
 */
function last(iterable $iter)
{
    $value = null;
    foreach ($iter as $value) {
        \assert(null !== $value, '$value is not null');
    }

    return $value;
}

/**
 * Calls a $fn on each element of an $iter.
 *
 * @template TValue
 *
 * @param iterable<TValue>       $iter
 * @param callable(TValue): void $fn
 */
function for_each(iterable $iter, callable $fn): void
{
    foreach ($iter as $value) {
        $fn($value);
    }
}

/**
 * @template TValue
 * @template TAcc
 *
 * @param iterable<TValue>             $iter
 * @param TAcc                         $init
 * @param callable(TAcc, TValue): TAcc $fn
 *
 * @return TAcc
 */
function fold(iterable $iter, $init, callable $fn)
{
    $acc = $init;
    foreach ($iter as $value) {
        $acc = $fn($_ = $acc, $value);
    }

    return $acc;
}

/**
 * Uses the first element in the $iter as the initial value, folding every subsequent element into it. An empty $iter returns null.
 *
 * @template TValue
 *
 * @param iterable<TValue>                 $iter
 * @param callable(TValue, TValue): TValue $fn
 *
 * @return ?TValue
 */
function fold_first(iterable $iter, callable $fn)
{
    $acc = null;
    foreach ($iter as $value) {
        \assert(null !== $value, '$value is not null');
        if (null === $acc) {
            $acc = $value;
        } else {
            $acc = $fn($_ = $acc, $value);
            \assert(null !== $acc, '$fn($acc, $value) is not null');
        }
    }

    return $acc;
}

/**
 * Tests if every element of the $iter matches a $predicate. An empty $iter returns true.
 *
 * @template TValue
 *
 * @param iterable<TValue>       $iter
 * @param callable(TValue): bool $predicate
 */
function all(iterable $iter, callable $predicate): bool
{
    foreach ($iter as $value) {
        if (!$predicate($value)) {
            return false;
        }
    }

    return true;
}

/**
 * Tests if any element of the $iter matches a $predicate. An empty $iter returns false.
 *
 * @template TValue
 *
 * @param iterable<TValue>       $iter
 * @param callable(TValue): bool $predicate
 */
function any(iterable $iter, callable $predicate): bool
{
    foreach ($iter as $value) {
        if ($predicate($value)) {
            return true;
        }
    }

    return false;
}

/**
 * Searches for an element of an $iter that satisfies a $predicate.
 *
 * @template TValue
 *
 * @param iterable<TValue>       $iter
 * @param callable(TValue): bool $predicate
 *
 * @return ?TValue
 */
function find(iterable $iter, callable $predicate)
{
    foreach ($iter as $value) {
        \assert(null !== $value, '$value is not null');
        if ($predicate($_ = $value)) {
            return $value;
        }
    }

    return null;
}

/**
 * Applies $fn to the elements of $iter and returns the first non-null result.
 *
 * @template TValue1
 * @template TValue2
 *
 * @param iterable<TValue1>           $iter
 * @param callable(TValue1): ?TValue2 $fn
 *
 * @return ?TValue2
 */
function find_map(iterable $iter, callable $fn)
{
    foreach ($iter as $value) {
        $value = $fn($_ = $value);
        if (null !== $value) {
            return $value;
        }
    }

    return null;
}

/**
 * Searches for an element in an $iter, returning its index.
 *
 * @template TValue
 *
 * @param iterable<TValue>       $iter
 * @param callable(TValue): bool $predicate
 */
function position(iterable $iter, callable $predicate): int
{
    $n = -1;
    foreach ($iter as $value) {
        ++$n;
        if ($predicate($value)) {
            return $n;
        }
    }

    return -1;
}

/**
 * Searches for an element in an $iter from the right, returning its index.
 *
 * @template TValue
 *
 * @param iterable<TValue>       $iter
 * @param callable(TValue): bool $predicate
 */
function rposition(iterable $iter, callable $predicate): int
{
    $n = $pos = -1;
    foreach ($iter as $value) {
        ++$n;
        if ($predicate($value)) {
            $pos = $n;
        }
    }

    return $pos;
}

/**
 * Returns the maximum element of an $iter.
 *
 * @template TValue
 *
 * @param iterable<TValue> $iter
 *
 * @return ?TValue
 */
function max(iterable $iter)
{
    $max = null;
    foreach ($iter as $value) {
        \assert(null !== $value, '$value is not null');
        if (null === $max || $max <= $value) {
            $max = $value;
        }
    }

    return $max;
}

/**
 * Returns the minimum element of an $iter.
 *
 * @template TValue
 *
 * @param iterable<TValue> $iter
 *
 * @return ?TValue
 */
function min(iterable $iter)
{
    $min = null;
    foreach ($iter as $value) {
        \assert(null !== $value, '$value is not null');
        if (null === $min || $min > $value) {
            $min = $value;
        }
    }

    return $min;
}

/**
 * Returns the element that gives the maximum value with respect to the specified $compare function.
 *
 * @template TValue
 *
 * @param iterable<TValue>              $iter
 * @param callable(TValue, TValue): int $compare
 *
 * @return ?TValue
 */
function max_by(iterable $iter, callable $compare)
{
    $max = null;
    foreach ($iter as $value) {
        \assert(null !== $value, '$value is not null');
        if (null === $max || $compare($_ = $max, $_ = $value) <= 0) {
            $max = $value;
        }
    }

    return $max;
}

/**
 * Returns the element that gives the minimum value with respect to the specified $compare function.
 *
 * @template TValue
 *
 * @param iterable<TValue>              $iter
 * @param callable(TValue, TValue): int $compare
 *
 * @return ?TValue
 */
function min_by(iterable $iter, callable $compare)
{
    $min = null;
    foreach ($iter as $value) {
        \assert(null !== $value, '$value is not null');
        if (null === $min || $compare($_ = $min, $_ = $value) > 0) {
            $min = $value;
        }
    }

    return $min;
}

/**
 * Returns the element that gives the maximum value from the specified $fn.
 *
 * @template TValue1
 * @template TValue2
 *
 * @param iterable<TValue1>          $iter
 * @param callable(TValue1): TValue2 $fn
 *
 * @return ?TValue1
 */
function max_by_key(iterable $iter, callable $fn)
{
    $max = [null, null];
    foreach ($iter as $value) {
        \assert(null !== $value, '$value is not null');
        $key = $fn($_ = $value);
        \assert(null !== $key, '$fn($value) is not null');
        if (null === $max[0] || $max[0] <= $key) {
            $max[0] = $key;
            $max[1] = $value;
        }
    }

    return $max[1];
}

/**
 * Returns the element that gives the minimum value from the specified $fn.
 *
 * @template TValue1
 * @template TValue2
 *
 * @param iterable<TValue1>          $iter
 * @param callable(TValue1): TValue2 $fn
 *
 * @return ?TValue1
 */
function min_by_key(iterable $iter, callable $fn)
{
    $min = [null, null];
    foreach ($iter as $value) {
        \assert(null !== $value, '$value is not null');
        $key = $fn($_ = $value);
        \assert(null !== $key, '$fn($value) is not null');
        if (null === $min[0] || $min[0] > $key) {
            $min[0] = $key;
            $min[1] = $value;
        }
    }

    return $min[1];
}

/**
 * Sums the elements of an $iter. An empty $iter returns 0.
 *
 * @param iterable<float|int> $iter
 *
 * @return float|int
 */
function sum(iterable $iter)
{
    $sum = 0;
    foreach ($iter as $value) {
        \assert(\is_numeric($value), '$value is number');
        $sum += $value;
    }

    return $sum;
}

/**
 * Multiplies the elements of an $iter. An empty $iter returns 1.
 *
 * @param iterable<float|int> $iter
 *
 * @return float|int
 */
function product(iterable $iter)
{
    $prod = 1;
    foreach ($iter as $value) {
        \assert(\is_numeric($value), '$value is number');
        $prod *= $value;
    }

    return $prod;
}

/**
 * Converts an $iter of pairs into a pair of arrays.
 *
 * @template TValue1
 * @template TValue2
 *
 * @param iterable<array{TValue1, TValue2}> $iter
 *
 * @return array{array<int, TValue1>, array<int, TValue2>}
 */
function unzip(iterable $iter): array
{
    $a = $b = [];
    foreach ($iter as [$a[], $b[]]);

    return [$a, $b];
}

/**
 * Lexicographically compares the elements of this $iter with those of another.
 *
 * @template TValue
 *
 * @param iterable<TValue> $iter
 * @param iterable<TValue> $other
 */
function cmp(iterable $iter, iterable $other): int
{
    $other = to_iterator($other);
    $other->rewind();

    foreach ($iter as $value) {
        if ($other->valid()) {
            $cmp = $value <=> $other->current();
            if (0 !== $cmp) {
                return $cmp;
            }
        } else {
            return 1;
        }
        $other->next();
    }

    return $other->valid() ? -1 : 0;
}

/**
 * Lexicographically compares the elements of this $iter with those of another with respect to the $compare function.
 *
 * @template TValue
 *
 * @param iterable<TValue>              $iter
 * @param iterable<TValue>              $other
 * @param callable(TValue, TValue): int $compare
 */
function cmp_by(iterable $iter, iterable $other, callable $compare): int
{
    $other = to_iterator($other);
    $other->rewind();

    foreach ($iter as $value) {
        if ($other->valid()) {
            $cmp = $compare($value, $other->current());
            if (0 !== $cmp) {
                return $cmp;
            }
        } else {
            return 1;
        }
        $other->next();
    }

    return $other->valid() ? -1 : 0;
}

/**
 * Determines if the elements of this $iter are equal to those of another.
 *
 * @template TValue
 *
 * @param iterable<TValue> $iter
 * @param iterable<TValue> $other
 */
function eq(iterable $iter, iterable $other): bool
{
    $other = to_iterator($other);
    $other->rewind();

    foreach ($iter as $value) {
        if (!$other->valid() || $value !== $other->current()) {
            return false;
        }
        $other->next();
    }

    return !$other->valid();
}

/**
 * Determines if the elements of this $iter are equal to those of another with respect to the $eq function.
 *
 * @template TValue
 *
 * @param iterable<TValue>               $iter
 * @param iterable<TValue>               $other
 * @param callable(TValue, TValue): bool $eq
 */
function eq_by(iterable $iter, iterable $other, callable $eq): bool
{
    $other = to_iterator($other);
    $other->rewind();

    foreach ($iter as $value) {
        if (!$other->valid() || !$eq($value, $other->current())) {
            return false;
        }
        $other->next();
    }

    return !$other->valid();
}

/**
 * Determines if the elements of this $iter are unequal to those of another.
 *
 * @template TValue
 *
 * @param iterable<TValue> $iter
 * @param iterable<TValue> $other
 */
function ne(iterable $iter, iterable $other): bool
{
    return !eq($iter, $other);
}

/**
 * Determines if the elements of this $iter are lexicographically less than those of another.
 *
 * @template TValue
 *
 * @param iterable<TValue> $iter
 * @param iterable<TValue> $other
 */
function lt(iterable $iter, iterable $other): bool
{
    return cmp($iter, $other) < 0;
}

/**
 * Determines if the elements of this $iter are lexicographically less or equal to those of another.
 *
 * @template TValue
 *
 * @param iterable<TValue> $iter
 * @param iterable<TValue> $other
 */
function le(iterable $iter, iterable $other): bool
{
    return cmp($iter, $other) <= 0;
}

/**
 * Determines if the elements of this $iter are lexicographically greater than those of another.
 *
 * @template TValue
 *
 * @param iterable<TValue> $iter
 * @param iterable<TValue> $other
 */
function gt(iterable $iter, iterable $other): bool
{
    return cmp($iter, $other) > 0;
}

/**
 * Determines if the elements of this $iter are lexicographically greater than or equal to those of another.
 *
 * @template TValue
 *
 * @param iterable<TValue> $iter
 * @param iterable<TValue> $other
 */
function ge(iterable $iter, iterable $other): bool
{
    return cmp($iter, $other) >= 0;
}

/**
 * Checks if the elements of this $iter are sorted.
 *
 * @template TValue
 *
 * @param iterable<TValue> $iter
 */
function is_sorted(iterable $iter): bool
{
    return is_sorted_by($iter, static fn ($a, $b): int => $a <=> $b);
}

/**
 * Checks if the elements of this $iter are sorted using the given $compare function.
 *
 * @template TValue
 *
 * @param iterable<TValue>              $iter
 * @param callable(TValue, TValue): int $compare
 */
function is_sorted_by(iterable $iter, callable $compare): bool
{
    $last = null;
    foreach ($iter as $value) {
        \assert(null !== $value, '$value is not null');
        if (null !== $last && $compare($_ = $last, $_ = $value) > 0) {
            return false;
        }
        $last = $value;
    }

    return true;
}

/**
 * Checks if the elements of this $iter are sorted using the given key extraction function.
 *
 * @template TValue1
 * @template TValue2
 *
 * @param iterable<TValue1>          $iter
 * @param callable(TValue1): TValue2 $fn
 */
function is_sorted_by_key(iterable $iter, callable $fn): bool
{
    return is_sorted(new MapIterator($iter, $fn));
}
