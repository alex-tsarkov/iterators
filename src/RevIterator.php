<?php

namespace AlexTsarkov\Iterators;

/**
 * An iterator with the direction inverted.
 *
 * @template TValue
 * @implements \IteratorAggregate<TValue>
 */
final class RevIterator implements \IteratorAggregate
{
    /**
     * @var iterable<TValue>
     */
    private iterable $iter;

    /**
     * @param iterable<TValue> $iter
     */
    public function __construct(iterable $iter)
    {
        $this->iter = $iter;
    }

    /**
     * @return \Iterator<TValue>
     */
    public function getIterator(): \Iterator
    {
        if ($this->iter instanceof self) {
            yield from $this->iter->iter;
        } elseif (\is_array($this->iter)) {
            yield from \array_reverse($this->iter, true);
        } else {
            $size = \is_countable($this->iter) ? \count($this->iter) : 16;
            $stack = new \SplFixedArray($size);

            $top = 0;
            foreach ($this->iter as $key => $value) {
                if ($top === $size) {
                    $size += $size < 1024 ? $size : 1024;
                    $stack->setSize($size);
                }
                $stack[$top++] = [$key, $value];
            }

            while (0 !== $top) {
                [$key, $value] = $stack[--$top];
                yield $key => $value;
            }
        }
    }
}
