<?php

namespace AlexTsarkov\Iterators;

/**
 * An iterator that repeats a $value endlessly.
 *
 * @template TValue
 * @implements \IteratorAggregate<int, TValue>
 */
final class RepeatIterator implements \IteratorAggregate
{
    /**
     * @var TValue
     */
    private $value;

    /**
     * @param TValue $value
     */
    public function __construct($value)
    {
        $this->value = $value;
    }

    /**
     * @return \Iterator<int, TValue>
     */
    public function getIterator(): \Iterator
    {
        while (true) {
            yield $this->value;
        }
        // @codeCoverageIgnoreStart
    }

    // @codeCoverageIgnoreEnd
}
