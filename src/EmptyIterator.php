<?php

namespace AlexTsarkov\Iterators;

/**
 * An iterator that yields nothing.
 *
 * @template TValue
 * @implements \IteratorAggregate<int, TValue>
 */
final class EmptyIterator implements \Countable, \IteratorAggregate
{
    public function count(): int
    {
        return 0;
    }

    /**
     * @return \Iterator<int, TValue>
     */
    public function getIterator(): \Iterator
    {
        return;
        yield; // @codeCoverageIgnore
    }
}
