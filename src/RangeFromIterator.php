<?php

namespace AlexTsarkov\Iterators;

/**
 * A range only bounded inclusively below.
 *
 * @implements \IteratorAggregate<int, int>
 */
final class RangeFromIterator implements \IteratorAggregate
{
    private int $start;

    public function __construct(int $start)
    {
        $this->start = $start;
    }

    /**
     * @return \Iterator<int, int>
     */
    public function getIterator(): \Iterator
    {
        for ($n = $this->start;; ++$n) {
            yield $n;
        }
        // @codeCoverageIgnoreStart
    }

    // @codeCoverageIgnoreEnd
}
