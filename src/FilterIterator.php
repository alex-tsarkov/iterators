<?php

namespace AlexTsarkov\Iterators;

/**
 * An iterator that filters the values of $iter with predicate $fn.
 *
 * @template TValue
 * @implements \IteratorAggregate<TValue>
 */
final class FilterIterator implements \IteratorAggregate
{
    /**
     * @var iterable<TValue>
     */
    private iterable $iter;

    /**
     * @var callable(TValue): bool
     */
    private $fn;

    /**
     * @param iterable<TValue>       $iter
     * @param callable(TValue): bool $fn
     */
    public function __construct(iterable $iter, callable $fn)
    {
        $this->iter = $iter;
        $this->fn = $fn;
    }

    /**
     * @return \Iterator<TValue>
     */
    public function getIterator(): \Iterator
    {
        foreach ($this->iter as $key => $value) {
            if (($this->fn)($_ = $value)) {
                yield $key => $value;
            }
        }
    }
}
