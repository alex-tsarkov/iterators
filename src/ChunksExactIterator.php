<?php

namespace AlexTsarkov\Iterators;

/**
 * An iterator over non-overlapping chunks of length $size.
 *
 * @template TValue
 * @implements \IteratorAggregate<int, array<int, TValue>>
 */
final class ChunksExactIterator implements \IteratorAggregate
{
    /**
     * @var iterable<TValue>
     */
    private iterable $iter;

    private int $size;

    /**
     * @param iterable<TValue> $iter
     */
    public function __construct(iterable $iter, int $size)
    {
        \assert($size > 0, "chunk \$size > 0, but {$size} given");

        $this->iter = $iter;
        $this->size = $size;
    }

    /**
     * @return \Iterator<int, array<int, TValue>>
     */
    public function getIterator(): \Iterator
    {
        $chunk = [];
        $n = 0;
        foreach ($this->iter as $value) {
            $chunk[$n++] = $value;
            if ($this->size === $n) {
                yield $chunk;
                $n = 0;
            }
        }
    }
}
