<?php

namespace AlexTsarkov\Iterators;

/**
 * An iterator that yields a single value by applying the provided $fn.
 *
 * @template TValue
 * @implements \IteratorAggregate<int, TValue>
 */
final class OnceWithIterator implements \Countable, \IteratorAggregate
{
    /**
     * @var callable(): TValue
     */
    private $fn;

    /**
     * @param callable(): TValue $fn
     */
    public function __construct(callable $fn)
    {
        $this->fn = $fn;
    }

    public function count(): int
    {
        return 1;
    }

    /**
     * @return \Iterator<int, TValue>
     */
    public function getIterator(): \Iterator
    {
        yield ($this->fn)();
    }
}
