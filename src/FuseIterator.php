<?php

namespace AlexTsarkov\Iterators;

/**
 * An iterator that iterates $iter until it yields null.
 *
 * @template TValue
 * @implements \IteratorAggregate<TValue>
 */
final class FuseIterator implements \IteratorAggregate
{
    /**
     * @var iterable<?TValue>
     */
    private iterable $iter;

    /**
     * @param iterable<?TValue> $iter
     */
    public function __construct(iterable $iter)
    {
        if ($iter instanceof self) {
            $iter = $iter->iter;
        }
        $this->iter = $iter;
    }

    /**
     * @return \Iterator<TValue>
     */
    public function getIterator(): \Iterator
    {
        foreach ($this->iter as $key => $value) {
            if (null === $value) {
                break;
            }
            yield $key => $value;
        }
    }
}
