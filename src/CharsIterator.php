<?php

namespace AlexTsarkov\Iterators;

/**
 * @see https://tools.ietf.org/html/rfc3629
 *
 * @implements \IteratorAggregate<int, int>
 */
final class CharsIterator implements \IteratorAggregate
{
    public const REPLACEMENT_CHARACTER = 0xFFFD;

    private const UTF8_CHAR_WIDTH = [
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, // 0x1F
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, // 0x3F
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, // 0x5F
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, // 0x7F
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // 0x9F
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // 0xBF
        0, 0, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, // 0xDF
        3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, // 0xEF
        4, 4, 4, 4, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // 0xFF
    ];

    private BytesIterator $iter;

    public function __construct(string $str)
    {
        $this->iter = new BytesIterator($str);
    }

    /**
     * @return \Iterator<int, int>
     */
    public function getIterator(): \Iterator
    {
        $bytes = $this->iter->getIterator();
        while ($bytes->valid()) {
            $key = $bytes->key();
            $char = $bytes->current();
            switch (self::UTF8_CHAR_WIDTH[$char]) {
                case 1:
                    $bytes->next();

                    break;
                case 2: case 3: case 4:
                    if ($this->tail($bytes, $char)) {
                        $bytes->next();
                    }

                    break;
                default:
                    $char = self::REPLACEMENT_CHARACTER;
                    $bytes->next();

                    break;
            }
            yield $key => $char;
        }
    }

    /**
     * @param \Iterator<int, int> $bytes
     */
    private function tail(\Iterator $bytes, int &$char): bool
    {
        $width = self::UTF8_CHAR_WIDTH[$char];
        $char &= 0b0111_1111 >> $width;
        while (0 !== --$width && $bytes->valid()) {
            $bytes->next();
            $b = $bytes->current();
            if (0b1000_0000 !== ($b & 0b1100_0000)) {
                break;
            }
            $char = ($char << 6) | ($b & 0b0011_1111);
        }

        if (0 !== $width) {
            $char = self::REPLACEMENT_CHARACTER;
        }

        return 0 === $width;
    }
}
