<?php

namespace AlexTsarkov\Iterators;

/**
 * An iterator that only iterates over the first $n values of $iter.
 *
 * @template TValue
 * @implements \IteratorAggregate<TValue>
 */
final class TakeIterator implements \IteratorAggregate
{
    /**
     * @var iterable<TValue>
     */
    private iterable $iter;

    private int $n;

    /**
     * @param iterable<TValue> $iter
     */
    public function __construct(iterable $iter, int $n)
    {
        \assert($n >= 0, "take \$n >= 0 values, but {$n} given");

        if ($iter instanceof self) {
            $n = \min($n, $iter->n);
            $iter = $iter->iter;
        }
        $this->iter = $iter;
        $this->n = $n;
    }

    /**
     * @return \Iterator<TValue>
     */
    public function getIterator(): \Iterator
    {
        $n = $this->n;
        foreach ($this->iter as $key => $value) {
            if (0 === $n) {
                break;
            }
            yield $key => $value;
            --$n;
        }
    }
}
